package com.teo.biplane24;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.palette.graphics.Palette;
import androidx.room.Room;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.teo.biplane24.database.AppDatabase;
import com.teo.biplane24.model.Filter;
import com.teo.biplane24.model.User;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class App extends Application {

    private static Cache mCache;
    private static OkHttpClient mOkHttpClient;
    private static Retrofit mRetrofit;
    private static RetrofitApi mRetrofitApi;
    private AppDatabase database;
    private static App mInstance;
    public static synchronized App getInstance() {
        return mInstance;
    }
    private Random rand;

    private Map<String, Integer> mapColors;

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    private Filter filter;

    public int getColorApp(String app){

        Integer color = mapColors.get(app);
        if (color != null){
            return color;
        }

        int colorInt = 0;

        for (int i = 0; i < 5; i++){
            int r = rand.nextInt(255);
            int g = rand.nextInt(255);
            int b = rand.nextInt(255);

            colorInt = Color.rgb(r, g, b);
        }


        Bitmap bitmap = getBitmapIcon(getAppIcon(app));
        if (bitmap != null) {
            Palette p = Palette.from(bitmap).generate();
            colorInt = p.getVibrantColor(colorInt);
        }

        mapColors.put(app, colorInt);

        return colorInt;
    }

    public Bitmap getBitmapIcon(Drawable icon) {
        Bitmap bitmap = null;
        try {
            if (icon instanceof BitmapDrawable) {
                bitmap = ((BitmapDrawable) icon).getBitmap();
            } else {
                bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }
        } catch (NullPointerException e) {
            //e.printStackTrace();
        }


        return bitmap;
    }

    public Drawable getAppIcon(String app) {
        try {
            Drawable icon = getPackageManager().getApplicationIcon(app);
            return icon;
        } catch (PackageManager.NameNotFoundException e) {
            //e.printStackTrace();
            return null;
        }
    }



    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        rand = new Random();

        mapColors = new HashMap<>();

        mapColors.put("transparent", getResources().getColor(android.R.color.transparent));

        initImageLoader(this);

        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("6111b32d-2104-4367-b414-e276d46c6393").build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);

        provideHttpCache();

        provideOkhttpClient();
        provideRetrofit();
        provideRetrofitApi();

        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        Intent intent = new Intent(this, AppLockService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }else {
            startService(intent);
        }

    }

    public User getUser(){
        List<User> list = getDatabase().getUserDao().getAll();
        if (list != null && list.size() == 1){
            User user = list.get(0);
            if (user.getToken() != null){
                return user;
            }

        }else if (list != null && list.size() > 1){
            getDatabase().getUserDao().deleteUsers(list);
        }

        return null;
    }

    public boolean isLogin(){
        return getUser() != null;
    }

    public AppDatabase getDatabase() {
        return database;
    }

    private void provideHttpCache() {
        int cacheSize = 10 * 1024 * 1024;
        mCache = new Cache(getApplicationContext().getCacheDir(), cacheSize);
    }

    private void provideOkhttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(mCache);
        client.followRedirects(false);
        client.addInterceptor(interceptor);
        mOkHttpClient = client.build();
    }

    private void provideRetrofit(){
        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://176.99.4.215")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(mOkHttpClient)
                .build();
    }

    private static void provideRetrofitApi(){
        mRetrofitApi = mRetrofit.create(RetrofitApi.class);
    }

    public static RetrofitApi getRetrofitApi() {
        return mRetrofitApi;
    }

    public static void initImageLoader(Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showStubImage(R.mipmap.ic_launcher)
                .resetViewBeforeLoading()
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);
    }
}
