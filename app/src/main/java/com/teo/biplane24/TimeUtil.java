package com.teo.biplane24;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeUnit;

public class TimeUtil {

    public static String convertMillis(long millis){

        long hour = TimeUnit.HOURS.toMillis(1);
        long minute = TimeUnit.MINUTES.toMillis(1);
        long second = TimeUnit.SECONDS.toMillis(1);


        long hours = 0;
        long minutes = 0;
        long seconds = 0;

        if (millis > hour){
            hours = millis / hour;

            long offset = millis - (hours * hour);
            if (offset > minute){
                minutes = offset / minute;

                offset = millis - (hours * hour) - (minutes * minute);
                if (offset > second){
                    seconds = offset / second;
                }
            }
        }else {
            minutes = millis / minute;

            long offset = millis - (minutes * minute);
            if (offset > second){
                seconds = offset / second;
            }
        }


        //String myTime =  String.format("%02d:%02d:%02d", hours, minutes, seconds);
        String myTime =  String.format("%02d:%02d", hours, minutes);

        return myTime;

    }



}
