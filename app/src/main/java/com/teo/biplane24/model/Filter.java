package com.teo.biplane24.model;

public class Filter {

    public long getFrom() {
        return from;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public long getTo() {
        return to;
    }

    public void setTo(long to) {
        this.to = to;
    }

    public String getTextPeriod() {
        return textPeriod;
    }

    public void setTextPeriod(String textPeriod) {
        this.textPeriod = textPeriod;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    private long from;
    private long to;
    private String textPeriod;
    private int group;
    private int status;
    private int place;
    private int round;
}
