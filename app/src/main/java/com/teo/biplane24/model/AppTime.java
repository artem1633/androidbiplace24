package com.teo.biplane24.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "time")
public class AppTime {

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    private String app;

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    private double lat;
    private double lng;
    @PrimaryKey
    private long startTime;

    private long stopTime;

}
