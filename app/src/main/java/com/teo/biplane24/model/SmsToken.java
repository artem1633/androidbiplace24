package com.teo.biplane24.model;

public class SmsToken {

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;
}
