package com.teo.biplane24.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.teo.biplane24.model.AppReport;
import com.teo.biplane24.model.AppTime;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.LocationReport;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.model.User;

import java.util.List;

@Dao
public abstract class UserDao {

    @Query("SELECT * FROM user")
    public abstract List<User> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(User wallpapers);

    @Update
    public abstract void update(User wallpapers);

    @Delete
    public abstract void deleteUsers(List<User> users);



    @Query("SELECT * FROM `group`")
    public abstract List<Group> getGroups();

    @Query("SELECT * FROM `group`  WHERE id =:app")
    public abstract Group getGroup(int app);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertGroup(Group wallpapers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertGroups(List<Group> groups);

    @Update
    public abstract void updateGroup(Group wallpapers);

    @Delete
    public abstract void deleteGroup(Group wallpapers);

    @Delete
    public abstract void deleteGroups(List<Group> groups);



    @Query("SELECT * FROM status")
    public abstract List<Status> getStatuses();

    @Query("SELECT * FROM status  WHERE id =:app")
    public abstract Status getStatus(int app);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertStatus(Status wallpapers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertStatuses(List<Status> wallpapers);

    @Update
    public abstract void updateStatus(Status wallpapers);

    @Delete
    public abstract void deleteStatus(Status wallpapers);

    @Delete
    public abstract void deleteStatuses(List<Status> statuses);




    @Query("SELECT * FROM place")
    public abstract List<Place> getPlaces();

    @Query("SELECT * FROM place  WHERE id =:app")
    public abstract Place getPlace(int app);

    @Query("SELECT * FROM place  WHERE user_id =:user")
    public abstract Place getPlaceUser(int user);

    @Query("SELECT * FROM place  WHERE id =:app")
    public abstract Place getPlaceId(int app);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertPlace(Place wallpapers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertPlaces(List<Place> wallpapers);

    @Update
    public abstract void updatePlace(Place wallpapers);

    @Delete
    public abstract void deletePlace(Place wallpapers);

    @Delete
    public abstract void deletePlaces(List<Place> places);



    @Query("SELECT * FROM report ORDER BY time_start ASC")
    public abstract List<Report> getReports();

    @Query("SELECT * FROM report WHERE user_id =:round_id ORDER BY time_start ASC")
    public abstract List<Report> getReports(int round_id);

    @Query("SELECT * FROM report WHERE time_start >=:start and time_start <:end ORDER BY time_start ASC")
    public abstract List<Report> getReports(long start, long end);

    @Query("SELECT * FROM report WHERE time_start >=:start and time_start <:end and app_id =:app ORDER BY time_start ASC")
    public abstract List<Report> getReports(long start, long end, String app);

    @Query("SELECT * FROM report WHERE place_id =:place ORDER BY time_start ASC")
    public abstract List<Report> getReportsPlace(int place);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertReport(Report report);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertReports(List<Report> reports);

    @Delete
    public abstract void deleteReports(List<Report> reports);



    @Query("SELECT * FROM round")
    public abstract List<Round> getRounds();

    @Query("SELECT * FROM round  WHERE href_user_id =:app")
    public abstract Round getRound(int app);

    @Query("SELECT * FROM round  WHERE id =:app")
    public abstract Round getRoundId(int app);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertRound(List<Round> rounds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertRound(Round round);

    @Delete
    public abstract void deleteRound(Round round);

    @Delete
    public abstract void deleteRounds(List<Round> groups);


    @Query("SELECT * FROM app_report")
    public abstract List<AppReport> getAppReports();
    @Delete
    public abstract void delete(AppReport round);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(AppReport round);

    @Query("SELECT * FROM location_report")
    public abstract List<LocationReport> getLocationReports();
    @Delete
    public abstract void delete(LocationReport round);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(LocationReport round);
}
