package com.teo.biplane24.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.teo.biplane24.model.AppReport;
import com.teo.biplane24.model.AppTime;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.LocationReport;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.model.User;

@Database(entities = {Report.class, User.class, Group.class, Status.class, Place.class, Round.class, AppReport.class, LocationReport.class}, version = 19)
@TypeConverters(MapTypeConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();
}
