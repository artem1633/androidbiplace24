package com.teo.biplane24.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;


public class MapTypeConverter {

    @TypeConverter
    public String fromMap(Map<String, String> map){
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        return gson.toJson(map, type);
    }

    @TypeConverter
    public Map<String, String> toMap(String text){
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        return gson.fromJson(text, type);
    }
}
