package com.teo.biplane24;

import android.database.Observable;

import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Share;
import com.teo.biplane24.model.SmsToken;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.model.UserToken;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface RetrofitApi {

    @GET
    Call<String> getSendSms(@Url String url, @Query("api_id") String api_id, @Query("to") String to, @Query("msg") String msg, @Query("json") int json);

    @GET("api/user/get-sms-token")
    Call<SmsToken> getSmsToken();

    @FormUrlEncoded
    @POST("api/user/register")
    Call<UserToken> register(@Field("login") String login);

    @GET("api/share-data/index")
    Call<List<Share>> getShareList(@Query("token") String token);


    @FormUrlEncoded
    @POST("api/share-data/create")
    Call<String> createShare(@Field("token") String token, @Field("week_days") String week_days,  @Field("time_from") String time_from,@Field("time_to") String time_to,@Field("comment") String comment);

    @GET("api/share-data/delete")
    Call<String> deleteShare(@Query("token") String token, @Query("id") int id);

    @GET("api/round/index")
    Call<List<Round>> getRoundList(@Query("token") String token);

    @FormUrlEncoded
    @POST("api/round/create")
    Call<Round> createRound(
            @Field("token") String token,
            @Field("status_id") int status_id,
            @Field("group_id") int group_id,
            @Field("href") String href,
            @Field("name") String name);

    @GET("api/round/delete")
    Call<String> deleteRound(@Query("token") String token, @Query("id") int id);

    @GET("api/group/index")
    Call<List<Group>> getGroups(@Query("token") String token);

    @FormUrlEncoded
    @POST("api/group/create")
    Call<Group> createGroup(@Field("token") String token, @Field("name") String name);

    @GET("api/status/index")
    Call<List<Status>> getStatuses(@Query("token") String token);

    @FormUrlEncoded
    @POST("api/status/create")
    Call<Status> createStatus(@Field("token") String token, @Field("name") String name);

    @GET("api/place/index")
    Call<List<Place>> getPlaceList(@Query("token") String token);

    @GET("api/place/delete")
    Call<String> deletePlace(@Query("token") String token, @Query("id") int id);

    @FormUrlEncoded
    @POST("api/place/create")
    Call<Place> createPlace(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("api/report/create")
    Call<Report> createReport(@FieldMap Map<String, String> map, @Field("time_start") long time_start, @Field("time_work") long time_work);

    @GET("api/report/search")
    Call<List<Report>> getReports(@Query("token") String token,
                            @Query("datetime_start") String datetime_start,
                            @Query("datetime_end") String datetime_end,
                            @QueryMap Map<String, Integer> map
    );

    @GET("api/report/index")
    Call<List<Report>> getReports(@Query("token") String token,
                                  @Query("datetime_start") String datetime_start,
                                  @Query("datetime_end") String datetime_end
    );

    @FormUrlEncoded
    @POST("api/location/create")
    Call<Report> createLocation(@FieldMap Map<String, String> map, @Field("time_start") long time_start, @Field("time_work") long time_work);

    @GET("api/location/search")
    Call<List<Report>> getLocations(@Query("token") String token,
                                  @Query("datetime_start") String datetime_start,
                                  @Query("datetime_end") String datetime_end,
                                  @QueryMap Map<String, Integer> map
    );

    @GET("api/location/index")
    Call<List<Report>> getLocations(@Query("token") String token,
                                  @Query("datetime_start") String datetime_start,
                                  @Query("datetime_end") String datetime_end
    );



}
