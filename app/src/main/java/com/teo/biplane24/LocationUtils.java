package com.teo.biplane24;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.teo.biplane24.model.Tochka;

import java.util.List;



public class LocationUtils implements LocationListener {

    private LocationManager locationManager;
    private String provider;
    private AppCompatActivity context;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    private Location location;
    private String text;


    public LocationUtils(AppCompatActivity context) {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

    }

    public LocationShangeListener getListener() {
        return listener;
    }

    public void setListener(LocationShangeListener listener) {
        this.listener = listener;
    }

    private LocationShangeListener listener;

    public interface LocationShangeListener{
        void onLocationChange();
        void onLocationInit();
    }

    @SuppressLint("MissingPermission")
    public boolean init(String text){
        this.text = text;
        if (checkRequestPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION, 100, text)){
            Criteria criteria = new Criteria();
            provider = locationManager.getBestProvider(criteria, false);
            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 , 10, this);
            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 , 10, this);
            if (locationManager.getAllProviders().contains(LocationManager.PASSIVE_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000 , 10, this);

            listener.onLocationInit();
            return true;
        }

        return false;
    }

    public boolean isChange() {
        return change;
    }

    public void setChange(boolean change) {
        this.change = change;
    }

    private boolean change = true;

    @Override
    public void onLocationChanged(Location location) {

            this.location = location;

            if (listener != null) {
                listener.onLocationChange();

            }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public boolean checkRequestPermissions(final AppCompatActivity context, final String permission, final int requestCode, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                if (TextUtils.isEmpty(text)){
                    context.requestPermissions(new String[]{permission}, requestCode);
                }else {
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.app_name)
                            .setMessage(text)
                            .setPositiveButton("Включить", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    context.requestPermissions(new String[]{permission}, requestCode);
                                }
                            })
                            .setNegativeButton("Отмена", null)
                            .show();
                }


                return false;
            }
        }
        return true;
    }

    @SuppressLint("MissingPermission")
    public void destroy(){
        locationManager.removeUpdates(this);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
            init(text);
        }
    }



}
