package com.teo.biplane24;

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;


import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.teo.biplane24.model.AppTime;
import com.teo.biplane24.model.AppReport;
import com.teo.biplane24.model.LocationReport;
import com.teo.biplane24.model.Report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppLockService extends AccessibilityService implements LocationListener {
    private static AppLockService sSharedInstance;

    String mCurrentPackage = "";
    String mLastPackageName = "";

    private BroadcastReceiver mScreenReceiver;
    private LocationManager locationManager;
    private String provider;

    public static Location getLocation() {
        return location;
    }

    private static Location location;

    private AppTime appTime;
    private AppTime appLocation;
    private String currentHomePackage;

    private static final String channelId = "vk_channel";
    private static final String channelName = "Channel vk";
    private NotificationChannel mChannel;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("tr", "create");
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);

        startCheckLocation();
        stop(false);
        stopLocation(false);
        handler.postDelayed(runnableLocation, 1000 * 60);
        createNotification(this, 1, "Фоновая работа");

        this.mScreenReceiver = new ScreenReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.SCREEN_ON");
        filter.addAction("android.intent.action.SCREEN_OFF");
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        //filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        //filter.addAction("android.net.wifi.STATE_CHANGE");

        registerReceiver(this.mScreenReceiver, filter);

    }

    private void createNotification(Context context, int id, String message) {

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        if (Build.VERSION.SDK_INT >= 26 ) {
            this.mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW);
            this.mChannel.enableVibration(false);
            this.mChannel.setVibrationPattern(null);
            this.mChannel.enableLights(false);
            notificationManager.createNotificationChannel(this.mChannel);
        }
        builder = new NotificationCompat.Builder(this, channelId);

        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(getResources().getString(R.string.app_name));
        builder.setContentText(message);
        builder.setAutoCancel(true);
        builder.setSound(null);
        builder.setContentIntent(contentIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(id, builder.build());
        }else {
            notificationManager.notify(id, builder.build());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        return Service.START_STICKY;
    }

    public void startCheckLocation(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
            if (locationManager.getAllProviders().contains(LocationManager.PASSIVE_PROVIDER))
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 1, this);
        }
    }

    private boolean screen;

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        //Log.e("tr", "location " + location.toString());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private final class ScreenReceiver extends BroadcastReceiver {
        private ScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            Log.e("tr", "receiver");
            if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
                startCheckLocation();

                Calendar calendar = Calendar.getInstance();
                lastApp = calendar.getTimeInMillis();

                if (appTime != null) {
                    appTime.setStartTime(lastApp);
                }

                screen = true;

                sendLocalData();

            } else if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                screen = false;
                handler.removeCallbacks(runnable);
                stop(true);
            }else if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){
                sendLocalData();
            }
        }
    }


    private void sendLocalData(){
        if (ConnectionUtil.isNetworkConnection(AppLockService.this)){
            List<LocationReport> appReportsLocation = App.getInstance().getDatabase().getUserDao().getLocationReports();

            for (LocationReport report : appReportsLocation) {
                App.getInstance().getDatabase().getUserDao().delete(report);
                createLocation(report);
            }

            List<AppReport> appReports = App.getInstance().getDatabase().getUserDao().getAppReports();

            for (AppReport report : appReports) {
                App.getInstance().getDatabase().getUserDao().delete(report);
                createReport(report);
            }
        }
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        sSharedInstance = this;
        screen = true;

        startCheckLocation();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        sSharedInstance = null;
        return super.onUnbind(intent);
    }

    public static AppLockService getSharedInstance() {
        return sSharedInstance;
    }

    private long lastApp;
    private long lastLocation;

    @SuppressLint("WrongConstant")
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event != null && event.getPackageName() != null) {
            this.mCurrentPackage = event.getPackageName().toString();


            Intent intentHome = new Intent(Intent.ACTION_MAIN);
            intentHome.addCategory(Intent.CATEGORY_HOME);
            ResolveInfo resolveInfo = getPackageManager().resolveActivity(intentHome, PackageManager.MATCH_DEFAULT_ONLY);
            currentHomePackage = resolveInfo.activityInfo.packageName;

            String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            List<InputMethodInfo> mInputMethodProperties = imm.getEnabledInputMethodList();

            final int N = mInputMethodProperties.size();

            for (int i = 0; i < N; i++) {

                InputMethodInfo imi = mInputMethodProperties.get(i);

                if (imi.getId().equals(Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD))) {
                    id = imi.getPackageName();
                    break;
                }
            }

            if (event.getEventType() == 32
                    && screen
                    && !this.mCurrentPackage.equals(id)
                    && !this.mCurrentPackage.equals(this.mLastPackageName)
                    && !this.mCurrentPackage.equals("com.android.systemui")) {


                handler.removeCallbacks(runnable);
                setLastPackageName(mCurrentPackage);
                handler.post(runnable);

            }
        }
    }


    public Handler handler = new Handler();

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (screen) {
                stop(true);

                appTime = new AppTime();
                appTime.setApp(mLastPackageName);
                appTime.setStartTime(lastApp);

                if (location != null) {
                    appTime.setLat(location.getLatitude());
                    appTime.setLng(location.getLongitude());
                }
            }

            handler.postDelayed(runnable, 1000 * 60);
        }
    };

    public Runnable runnableLocation = new Runnable() {
        @Override
        public void run() {


                stopLocation(true);

                appLocation = new AppTime();
                appLocation.setStartTime(lastLocation);


            handler.postDelayed(runnableLocation, 1000 * 60);
        }
    };

    public void stopLocation(boolean send) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Calendar calendar = Calendar.getInstance();

        long stopTime = calendar.getTimeInMillis();

        String datetime = format.format(new Date(stopTime));
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            day = 7;
        } else {
            day -= 1;
        }

        if (lastLocation > 0 && send) {

            if (appLocation != null) {
                appLocation.setStopTime(stopTime);

                Map<String, String> map = new HashMap<>();
                map.put("token", App.getInstance().getUser().getToken());
                map.put("datetime", datetime);
                map.put("week_day", String.valueOf(day));
                if (appLocation.getLat() == 0 && appLocation.getLng() == 0  && location != null) {
                    appLocation.setLat(location.getLatitude());
                    appLocation.setLng(location.getLongitude());

                }
                if (appLocation.getLng() != 0 && appLocation.getLat() != 0){
                    map.put("coords", appLocation.getLat() + "," + appLocation.getLng());

                    long time_work = appLocation.getStopTime() - appLocation.getStartTime();
                    if (time_work > 60000){
                        time_work = 60000;
                    }

                    long start_time = appLocation.getStartTime() / 1000;

                    sendReportLocation(start_time, time_work, map);
                }

            }
        }

        lastLocation = stopTime;
    }

    private void sendReportLocation(long timeStart, long timeWork, Map<String, String> map) {

        LocationReport appReport = new LocationReport();
        appReport.setMap(map);
        appReport.setStartTime(timeStart);
        appReport.setWorkTime(timeWork);
        //App.getInstance().getDatabase().getUserDao().insert(appReport);

        createLocation(appReport);

    }

    private void createLocation(LocationReport report){
        Call<Report> call = App.getRetrofitApi().createLocation(report.getMap(), report.getStartTime(), report.getWorkTime() / 1000);

        call.enqueue(new Callback<Report>() {
            @Override
            public void onResponse(Call<Report> call, Response<Report> response) {
                if (response != null && response.body() != null) {

                }else {
                    App.getInstance().getDatabase().getUserDao().insert(report);
                }

            }

            @Override
            public void onFailure(Call<Report> call, Throwable t) {
                App.getInstance().getDatabase().getUserDao().insert(report);
            }
        });
    }


    public void stop(boolean send) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Calendar calendar = Calendar.getInstance();

        long stopTime = calendar.getTimeInMillis();

        String datetime = format.format(new Date(stopTime));
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            day = 7;
        } else {
            day -= 1;
        }


        if (lastApp > 0 && send) {

            if (appTime != null && !TextUtils.isEmpty(currentHomePackage) && !appTime.getApp().contains(currentHomePackage)) {
                appTime.setStopTime(stopTime);

                Map<String, String> map = new HashMap<>();
                map.put("token", App.getInstance().getUser().getToken());
                map.put("app_id", appTime.getApp());
                map.put("datetime", datetime);
                map.put("week_day", String.valueOf(day));

                long time_work = appTime.getStopTime() - appTime.getStartTime();
                if (time_work > 60000){
                    time_work = 60000;
                }

                long start_time = appTime.getStartTime() / 1000;
                Log.e("tr", "START " + appTime.getStartTime() + " " + start_time);

                sendReport(start_time, time_work, map);
            }
        }

        lastApp = stopTime;
    }


    private void sendReport(long timeStart, long timeWork, Map<String, String> map) {

        AppReport appReport = new AppReport();
        appReport.setMap(map);
        appReport.setStartTime(timeStart);
        appReport.setWorkTime(timeWork);

        createReport(appReport);
    }


    private void createReport(AppReport report){
        Call<Report> call = App.getRetrofitApi().createReport(report.getMap(), report.getStartTime(), report.getWorkTime() / 1000);

        call.enqueue(new Callback<Report>() {
            @Override
            public void onResponse(Call<Report> call, Response<Report> response) {
                if (response != null && response.body() != null) {

                }else {
                    App.getInstance().getDatabase().getUserDao().insert(report);
                }

            }

            @Override
            public void onFailure(Call<Report> call, Throwable t) {
                App.getInstance().getDatabase().getUserDao().insert(report);
            }
        });
    }


    public void setLastPackageName(String packageName) {
        this.mLastPackageName = packageName;
    }

    @Override
    public void onDestroy() {
        Log.e("tr", "destroy");
        super.onDestroy();
    }


    public void onInterrupt() {
        if (this.mScreenReceiver != null) {
            unregisterReceiver(this.mScreenReceiver);
        }
    }

}
