package com.teo.biplane24.ui.report.users;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.FilterActivity;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.BaseReportFragment;
import com.teo.biplane24.ui.report.adapters.ReportUserAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportUsersFragment extends BaseReportFragment implements UsersBaseChartHelper.OnCharResultListener{

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.item_sum)
    TextView textViewSum;

    @BindView(R.id.item_period)
    TextView textPeriod;
    @BindView(R.id.chart_inflater)
    LinearLayout layoutChar;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;
    @BindView(R.id.item_error)
    TextView textError;
    public ReportUserAdapter appAdapter;

    private UsersBaseChartHelper charHelper;

    private SharedPreferences preferences;
    private int type;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_report, container, false);
        ButterKnife.bind(this, view);

        return view;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
        textPeriod.setText(filter.getTextPeriod());

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        appAdapter = new ReportUserAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(appAdapter);

        appAdapter.setListener(new ReportUserAdapter.AppClickListener() {
            @Override
            public void onAppClick(UsedApp item) {
                Intent intent = new Intent(getContext(), UserReportActivity.class);
                intent.putExtra("app", item);
                intent.putExtra("from", filter.getFrom());
                intent.putExtra("to", filter.getTo());
                startActivity(intent);

            }
        });

        getData();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK){
            if (data != null){
                textPeriod.setText(filter.getTextPeriod());
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void getData() {
        if (getActivity() != null){
            filter = App.getInstance().getFilter();
            textPeriod.setText(filter.getTextPeriod());
            appAdapter.clear();

            layoutChar.removeAllViews();
            textError.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            getReports(new OnReportListener() {
                @Override
                public void onSuccess() {
                    getLocalData();
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure() {
                    if (getActivity() != null){
                        progressBar.setVisibility(View.GONE);
                        textError.setVisibility(View.VISIBLE);
                    }
                }
            });
        }


    }

    @Override
    public void getLocalData() {
        if (getActivity() != null){
            int type = preferences.getInt("type", 1);
            progressBar.setVisibility(View.VISIBLE);
            layoutChar.removeAllViews();

            switch (type){
                case 1:
                    charHelper = new UserStackedHelper(getContext(), layoutChar, ReportUsersFragment.this);
                    break;
                case 2:
                    charHelper = new UsersLineHelper(getContext(), layoutChar, ReportUsersFragment.this);
                    break;
                case 3:
                    charHelper = new UsersPieHelper(getContext(), layoutChar, ReportUsersFragment.this);
                    break;
            }

            charHelper.setProgress(progressBar);
            charHelper.getDataUsers(0, filter.getFrom(), filter.getTo());
        }

    }



    @Override
    public void onCharResult(String textSum, List<UsedApp> list) {
        if (getActivity() != null){
            if (list == null) {
                list = new ArrayList<>();
            }
            textViewSum.setText(textSum);
            appAdapter.setList(list);
            progressBar.setVisibility(View.GONE);
        }
    }
}
