package com.teo.biplane24.ui.place;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.ui.place.adapters.PlaceAdapter;
import com.teo.biplane24.ui.round.AddRoundActivity;
import com.teo.biplane24.ui.round.adapters.RoundAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceFragment extends Fragment {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private PlaceAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_points, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PlaceAdapter(getContext());
        recyclerView.setAdapter(adapter);


        adapter.setListener(new PlaceAdapter.PlaceListener() {

            @Override
            public void onDelete(Place share) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.app_name)
                        .setMessage("Удалить?")
                        .setPositiveButton("ок", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Call<String> call = App.getRetrofitApi().deletePlace(App.getInstance().getUser().getToken(), share.getId());
                                call.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        if (response != null && response.body() != null){
                                            App.getInstance().getDatabase().getUserDao().deletePlace(share);
                                            adapter.getList().remove(share);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {

                                    }
                                });
                            }
                        })
                        .setNegativeButton("Отмена", null)
                        .show();
            }
        });
    }

    @Override
    public void onResume() {

        super.onResume();

        List<Place> list = App.getInstance().getDatabase().getUserDao().getPlaces();
        adapter.setList(list);
    }

    @OnClick(R.id.button_add)
    public void add(){

        startActivity(new Intent(getContext(), AddPlaceActivity.class));
    }
}