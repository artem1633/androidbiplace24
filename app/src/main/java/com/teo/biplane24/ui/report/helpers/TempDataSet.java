package com.teo.biplane24.ui.report.helpers;

import java.util.List;

public class TempDataSet {

    public TempDataSet(){

    }
    public TempDataSet(List<TempApp> list, String appName) {
        this.list = list;
        this.appName = appName;
    }

    public List<String> getAppNames() {
        return appNames;
    }

    public float[] getYs() {
        return ys;
    }

    public void setYs(float[] ys) {
        this.ys = ys;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    private int period;
    private float[] ys;
    public void setAppNames(List<String> appNames) {
        this.appNames = appNames;
    }

    private List<String> appNames;

    public List<TempApp> getList() {
        return list;
    }

    public void setList(List<TempApp> list) {
        this.list = list;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    private List<TempApp> list;
    private String appName;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    private int color;
}
