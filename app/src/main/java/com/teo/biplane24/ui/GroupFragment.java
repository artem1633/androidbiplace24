package com.teo.biplane24.ui;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Status;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupFragment extends Fragment {

    @BindView(R.id.item_spinner)
    Spinner spinner;
    @BindView(R.id.item_add)
    ImageView buttonAdd;
    @BindView(R.id.layout_select)
    LinearLayout layoutSelect;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragent_group, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private List<Group> list;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        list = new ArrayList<>();
        Group status = new Group();
        status.setName("Все группы");
        status.setId(0);
        list.add(status);
        list.addAll(App.getInstance().getDatabase().getUserDao().getGroups());


        layoutSelect.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        ArrayAdapter<Group> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(arrayAdapter);

        int select = App.getInstance().getFilter().getGroup();
        for (int i = 0; i < list.size(); i++){
            Group item = list.get(i);
            if (item.getId() == select){
                select = i;
            }
        }
        if (select < list.size()) {
            spinner.setSelection(select);
        }

    }


    public Group getSelect() {

        return (Group) spinner.getSelectedItem();

    }

    @OnClick(R.id.item_add)
    public void add() {
        EditText editText = new EditText(getContext());
        new AlertDialog.Builder(getContext())
                .setTitle("Создать группу")
                .setView(editText)
                .setPositiveButton("Создать", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String text = editText.getText().toString();
                        if (!TextUtils.isEmpty(text)) {

                            layoutSelect.setVisibility(View.GONE);
                            progressBar.setVisibility(View.VISIBLE);
                            Call<Group> call = App.getRetrofitApi().createGroup(App.getInstance().getUser().getToken(), text);
                            call.enqueue(new Callback<Group>() {
                                @Override
                                public void onResponse(Call<Group> call, Response<Group> response) {
                                    if (response != null && response.body() != null) {
                                        layoutSelect.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.GONE);
                                        list.add(response.body());
                                        App.getInstance().getDatabase().getUserDao().insertGroup(response.body());

                                        ArrayAdapter<Group> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
                                        spinner.setAdapter(arrayAdapter);
                                    }
                                }

                                @Override
                                public void onFailure(Call<Group> call, Throwable t) {

                                }
                            });
                        }
                    }
                })
                .show();
    }
}
