package com.teo.biplane24.ui.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.appcompat.app.AppCompatActivity;

import com.teo.biplane24.App;
import com.teo.biplane24.MainActivity;
import com.teo.biplane24.R;
import com.teo.biplane24.database.UserDao;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.SmsToken;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.model.User;
import com.teo.biplane24.model.UserToken;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.layout_code)
    RelativeLayout layoutCode;
    @BindView(R.id.layout_splash)
    LinearLayout layoutSplash;
    @BindView(R.id.layout_phone)
    LinearLayout layoutPhone;
    @BindView(R.id.item_phone)
    EditText editPhone;
    @BindView(R.id.item_code)
    EditText editCode;
    @BindView(R.id.button_send_code)
    Button sendCode;
    @BindView(R.id.check_politic)
    CheckBox checkBox;

    @BindView(R.id.button_send_phone)
    Button sendPhone;

    private String code;
    private String smsToken;
    private String phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //App.getInstance().getDatabase().getUserDao().delete(App.getInstance().getUser());
        if (App.getInstance().isLogin()){
            getGroups();


        }else {
            Call<SmsToken> call = App.getRetrofitApi().getSmsToken();
            call.enqueue(new Callback<SmsToken>() {
                @Override
                public void onResponse(Call<SmsToken> call, Response<SmsToken> response) {
                    if (response!= null && response.body() != null){
                        smsToken = response.body().getToken();
                        layoutPhone.setVisibility(View.VISIBLE);
                        layoutSplash.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<SmsToken> call, Throwable t) {

                }
            });
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sendCode.setEnabled(b);
            }
        });
    }


    @OnClick(R.id.privacy_politic)
    public void politic(){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://biplane24.ru/timestream/"));
        startActivity(intent);
    }

    private void getGroups(){
        UserDao userDao = App.getInstance().getDatabase().getUserDao();
        userDao.deleteGroups(userDao.getGroups());
        userDao.deletePlaces(userDao.getPlaces());
        userDao.deleteRounds(userDao.getRounds());
        userDao.deleteStatuses(userDao.getStatuses());

        Call<List<Group>> call = App.getRetrofitApi().getGroups(App.getInstance().getUser().getToken());
        call.enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response != null && response.body() != null){


                    userDao.insertGroups(response.body());

                    Call<List<Status>> calls = App.getRetrofitApi().getStatuses(App.getInstance().getUser().getToken());
                    calls.enqueue(new Callback<List<Status>>() {
                        @Override
                        public void onResponse(Call<List<Status>> call, Response<List<Status>> response) {
                            if (response != null && response.body() != null){
                                userDao.insertStatuses(response.body());

                                Call<List<Place>> callPlace = App.getRetrofitApi().getPlaceList(App.getInstance().getUser().getToken());
                                callPlace.enqueue(new Callback<List<Place>>() {
                                    @Override
                                    public void onResponse(Call<List<Place>> call, Response<List<Place>> response) {
                                        if (response != null && response.body() != null){
                                            userDao.insertPlaces(response.body());

                                            Call<List<Round>> callRound = App.getRetrofitApi().getRoundList(App.getInstance().getUser().getToken());
                                            callRound.enqueue(new Callback<List<Round>>() {
                                                @Override
                                                public void onResponse(Call<List<Round>> call, Response<List<Round>> response) {
                                                    if (response != null && response.body() != null){
                                                        App.getInstance().getDatabase().getUserDao().insertRound(response.body());
                                                        Round round = new Round();
                                                        round.setHref_user_id(App.getInstance().getUser().getId());
                                                        round.setId(App.getInstance().getUser().getId());
                                                        round.setName("Я");
                                                        App.getInstance().getDatabase().getUserDao().insertRound(round);
                                                        startMain();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<List<Round>> call, Throwable t) {

                                                }
                                            });

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<List<Place>> call, Throwable t) {

                                    }
                                });

                            }
                        }

                        @Override
                        public void onFailure(Call<List<Status>> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {

            }
        });
    }

    /*
    [{"id":4,"user_id":4,"href_user_id":8,"href":"http://biplane24.teo-crm.com/ZxGgFu05","name":"вера","status_id":2,"group_id":2},
    {"id":5,"user_id":4,"href_user_id":9,"href":"http://biplane24.teo-crm.com/wq3GyuKs","name":"саша","status_id":2,"group_id":2},
    {"id":6,"user_id":4,"href_user_id":8,"href":"http://biplane24.teo-crm.com/ZxGgFu05","name":"верони","status_id":2,"group_id":2},
    {"id":7,"user_id":4,"href_user_id":4,"href":"http://biplane24.teo-crm.com/Zovt0Jm3","name":"тест Коля","status_id":2,"group_id":2},
    {"id":8,"user_id":4,"href_user_id":4,"href":"http://biplane24.teo-crm.com/Zovt0Jm3","name":"коля 2","status_id":2,"group_id":2}]

     */

    @OnClick(R.id.button_send_phone)
    public void sendPhone(){
        sendPhone.setEnabled(false);
        phone = editPhone.getText().toString();
        final int min = 1000;
        final int max = 9999;
        final int random = new Random().nextInt((max - min) + 1) + min;
        //Log.e("tr", "sms " + random);
        code = String.valueOf(random);

        //editCode.setText(code);
        //layoutCode.setVisibility(View.VISIBLE);
        //layoutPhone.setVisibility(View.GONE);

        Call<String> call = App.getRetrofitApi().getSendSms("https://sms.ru/sms/send", smsToken, phone, code, 1);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response != null && response.body() != null){
                    layoutCode.setVisibility(View.VISIBLE);
                    layoutPhone.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                sendPhone.setEnabled(false);
            }
        });

    }

    @OnClick(R.id.button_send_code)
    public void login(){
        sendCode.setEnabled(false);
        String code = editCode.getText().toString();
        if (code.equals(this.code)){
            Call<UserToken> call = App.getRetrofitApi().register(phone);
            call.enqueue(new Callback<UserToken>() {
                @Override
                public void onResponse(Call<UserToken> call, Response<UserToken> response) {
                    if (response != null && response.body() != null){
                        if (response.body().getResult() != null){
                            User user = new User();
                            user.setToken(response.body().getResult());
                            user.setId(response.body().getId());
                            App.getInstance().getDatabase().getUserDao().insert(user);
                            getGroups();
                        }else if (response.body().getLogin() != null){
                            Toast.makeText(LoginActivity.this, response.body().getLogin(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserToken> call, Throwable t) {
                    sendCode.setEnabled(true);
                }
            });
        }
    }

    private void startMain(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
