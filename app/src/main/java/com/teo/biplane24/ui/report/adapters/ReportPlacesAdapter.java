package com.teo.biplane24.ui.report.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.UsedApp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportPlacesAdapter extends RecyclerView.Adapter<ReportPlacesAdapter.ViewHolder>{

    public List<UsedApp> getList() {
        return list;
    }

    public void setList(List<UsedApp> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private int type;

    private List<UsedApp> list;
    public ReportPlacesAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    private Context context;

    @NonNull
    @Override
    public ReportPlacesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_place_round, parent, false);
        return new ReportPlacesAdapter.ViewHolder(view);
    }

    public PlaceClickListener getListener() {
        return listener;
    }

    public void setListener(PlaceClickListener listener) {
        this.listener = listener;
    }

    private PlaceClickListener listener;


    public interface PlaceClickListener{
        void onPlaceClick(UsedApp item);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportPlacesAdapter.ViewHolder holder, int position) {

        final UsedApp item = list.get(position);

        int p = position + 1;
        holder.textPosition.setText(String.valueOf(p));
        holder.textName.setText(item.getName());
        holder.textTime.setText(TimeUtil.convertMillis(item.getUsedTime()));

        if (!TextUtils.isEmpty(item.getGroup())){
            holder.textGroup.setText("Группа - " + item.getGroup());
        }
        if (!TextUtils.isEmpty(item.getStatus())){
            holder.textStatus.setText("Статус - " + item.getStatus());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPlaceClick(item);
            }
        });

        if (type == 1){
            holder.imageColor.setVisibility(View.VISIBLE);
            holder.imageColor.setColorFilter(item.getColor());
        }else {
            holder.imageColor.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_color)
        ImageView imageColor;
        @BindView(R.id.item_position)
        TextView textPosition;
        @BindView(R.id.item_name)
        TextView textName;
        @BindView(R.id.item_time)
        TextView textTime;
        @BindView(R.id.item_status)
        TextView textStatus;
        @BindView(R.id.item_group)
        TextView textGroup;
        @BindView(R.id.item_period)
        TextView textPeriod;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
