package com.teo.biplane24.ui.report;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayout;
import com.teo.biplane24.App;
import com.teo.biplane24.FilterActivity;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Filter;
import com.teo.biplane24.ui.report.apps.ReportAppFragment;
import com.teo.biplane24.ui.report.places.ReportPlacesFragment;
import com.teo.biplane24.ui.report.users.ReportUsersFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportFragment extends Fragment {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.spinner_period)
    Spinner spinnerPeriod;
    @BindView(R.id.spinner_type)
    Spinner spinnerType;
    @BindView(R.id.spinner_index)
    Spinner spinnerIndex;

    private BaseReportFragment oldFragment;
    private SharedPreferences preferences;

    private String[] listIndex = new String[]{"Время присутствия людей", "Количество человек"};
    private String[] listType = new String[]{"Столбцы", "Линии", "Круговая"};
    private String[] listPeriod = new String[]{"каждые 10минут - (в минутах)", "по часам (в минутах)", "по дням (в часах)", "по неделям (в часах)", "по месяцам (в часах)"};

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    private int initPeriod = 0;
    private int initType = 0;
    private int initIndex = 0;

    public long from;
    public long to;

    private String textPeriod;

    public int group, status, place, round;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        ArrayAdapter<String> adapterType = new ArrayAdapter<>(getContext(), R.layout.item_spinner, listType);
        spinnerType.setAdapter(adapterType);

        ArrayAdapter<String> adapterPeriod = new ArrayAdapter<>(getContext(), R.layout.item_spinner, listPeriod);
        spinnerPeriod.setAdapter(adapterPeriod);

        ArrayAdapter<String> adapterIndex = new ArrayAdapter<>(getContext(), R.layout.item_spinner, listIndex);
        spinnerIndex.setAdapter(adapterIndex);


        spinnerPeriod.setSelection(preferences.getInt("group_period_s", 0));
        spinnerType.setSelection(preferences.getInt("group_type_s", 0));
        spinnerIndex.setSelection(preferences.getInt("group_index_s", 0));

        spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++initPeriod > 1){
                    long period = getPeriod();

                    preferences.edit().putLong("period", period).apply();

                    oldFragment.getLocalData();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++initType > 1){
                    int type = getType();
                    Log.e("tr", "select spinnerType");
                    preferences.edit().putInt("type", type).apply();

                    oldFragment.getLocalData();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinnerIndex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (++initIndex > 1){
                    Log.e("tr", "select spinnerIndex");
                    preferences.edit().putInt("group_index_s", i).apply();
                    oldFragment.getLocalData();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        setTime();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                BaseReportFragment fragment = null;
                switch (tab.getPosition()){
                    case 0:
                        getType();
                        spinnerIndex.setVisibility(View.GONE);
                        spinnerType.setVisibility(View.VISIBLE);

                        fragment = new ReportAppFragment();
                        break;
                    case 1:
                        getType();
                        spinnerIndex.setVisibility(View.GONE);
                        spinnerType.setVisibility(View.VISIBLE);
                        fragment = new ReportUsersFragment();
                        break;
                    case 2:
                        spinnerPeriod.setVisibility(View.VISIBLE);
                        spinnerIndex.setVisibility(View.VISIBLE);
                        spinnerType.setVisibility(View.GONE);
                        fragment = new ReportPlacesFragment();
                        break;
                }

                if (fragment != null){

                    oldFragment = fragment;

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.view_pager, fragment)
                            .commitAllowingStateLoss();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.addTab(tabLayout.newTab().setText(titles[0]), true);
        tabLayout.addTab(tabLayout.newTab().setText(titles[1]));
        tabLayout.addTab(tabLayout.newTab().setText(titles[2]));

    }

    private void setTime(){


        Filter filter = new Filter();

        Calendar calendar = Calendar.getInstance();
        long date = calendar.getTimeInMillis();
        from = date - TimeUnit.DAYS.toMillis(1);
        to = date;


        textPeriod = getFormatDate(from) + " - " + getFormatDate(to);



        calendar = Calendar.getInstance();
        date = calendar.getTimeInMillis();
        from = date - TimeUnit.DAYS.toMillis(1);
        to = date;

        filter.setFrom(from);
        filter.setTo(to);
        filter.setTextPeriod(textPeriod);

        App.getInstance().setFilter(filter);

    }

    public String getFormatDate(long date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String fo =  format.format(new Date(date));

        return fo;
    }

    public String getFormatDateUTC(long date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String fo =  format.format(new Date(date));

        return fo;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == AppCompatActivity.RESULT_OK){
            if (data != null){

                Filter filter = new Filter();

                from = data.getLongExtra("from", 0);
                to = data.getLongExtra("to", 0);
                textPeriod = getFormatDate(from) + " - " + getFormatDate(to);

                group = data.getIntExtra("group", 0);
                status = data.getIntExtra("status", 0);
                place = data.getIntExtra("place", 0);
                round = data.getIntExtra("round", 0);

                filter.setFrom(from);
                filter.setTo(to);
                filter.setTextPeriod(textPeriod);
                filter.setGroup(group);
                filter.setPlace(place);
                filter.setRound(round);
                filter.setStatus(status);

                App.getInstance().setFilter(filter);

                oldFragment.getData();
            }
        }

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_filter:
                Intent intent = new Intent(getActivity(), FilterActivity.class);
                intent.putExtra("from", from);
                intent.putExtra("to", to);
                intent.putExtra("tab", tabLayout.getSelectedTabPosition());
                startActivityForResult(intent, 200);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private long getPeriod(){
        long period = 0;
        preferences.edit().putInt("group_period_s", spinnerPeriod.getSelectedItemPosition()).apply();
        switch (spinnerPeriod.getSelectedItemPosition()){
            case 0:
                period = TimeUnit.MINUTES.toMillis(10);
                break;
            case 1:
                period = TimeUnit.HOURS.toMillis(1);
                break;
            case 2:
                period = TimeUnit.DAYS.toMillis(1);
                break;
            case 3:
                period = TimeUnit.DAYS.toMillis(7);
                break;
            case 4:
                period = TimeUnit.DAYS.toMillis(31);
                break;

        }

        return period;
    }

    private int getType(){
        int period = 0;
        preferences.edit().putInt("group_type_s", spinnerType.getSelectedItemPosition()).apply();
        switch (spinnerType.getSelectedItemPosition()){
            case 0:
                period = 1;
                spinnerPeriod.setVisibility(View.VISIBLE);

                break;
            case 1:
                period = 2;
                spinnerPeriod.setVisibility(View.VISIBLE);
                break;
            case 2:
                period = 3;
                spinnerPeriod.setVisibility(View.GONE);
                break;


        }

        return period;
    }



    private int[] titles = new int[]{R.string.title_report_1,R.string.title_report_2, R.string.title_report_3 };


}