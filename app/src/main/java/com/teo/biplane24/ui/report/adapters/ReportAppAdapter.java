package com.teo.biplane24.ui.report.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.UsedApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportAppAdapter extends RecyclerView.Adapter<ReportAppAdapter.ViewHolder>{

    public List<UsedApp> getList() {
        return list;
    }

    public void setList(List<UsedApp> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<UsedApp> list;
    public ReportAppAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    private Context context;

    public AppClickListener getListener() {
        return listener;
    }

    public void setListener(AppClickListener listener) {
        this.listener = listener;
    }

    private AppClickListener listener;
    public interface AppClickListener{
        void onAppClick(UsedApp item);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_app, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final UsedApp item = list.get(position);
        holder.textName.setText(item.getName());
        final Drawable icon = getAppIcon(item.getApp());
        if (icon == null){
            holder.imageView.setImageResource(R.drawable.ic_app);
        }else {
            holder.imageView.setImageDrawable(icon);
        }
        holder.textTime.setText(TimeUtil.convertMillis(item.getUsedTime()));

        holder.imageColor.setColorFilter(item.getColor());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAppClick(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_image)
        ImageView imageView;
        @BindView(R.id.item_color)
        ImageView imageColor;
        @BindView(R.id.item_name)
        TextView textName;
        @BindView(R.id.item_time)
        TextView textTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public Drawable getAppIcon(String app){
        try {
            Drawable icon = context.getPackageManager().getApplicationIcon(app);
            return icon;
        }
        catch (PackageManager.NameNotFoundException e) {

            return null;
        }
    }

}
