package com.teo.biplane24.ui.report.apps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.ui.MyMarkerView;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.util.ArrayList;
import java.util.List;

public class AppsLineHelper extends AppsBaseChartHelper {

    private LineChart chart;


    public AppsLineHelper(Context context, LinearLayout root, OnCharResultListener listener) {
        super(context, listener);

        chart = new LineChart(context);
        chart.setNoDataText(context.getResources().getString(R.string.char_empty));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        root.addView(chart, params);

        chart.setOnChartValueSelectedListener(this);

        chart.setDrawGridBackground(false);
        chart.getDescription().setEnabled(false);
        chart.setDrawBorders(false);

        MyMarkerView mv = new MyMarkerView(context, R.layout.custom_marker_view);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = chart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setLabelCount(labelCount);

        xAxis.setCenterAxisLabels(false);

        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return getFormatX(value);
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                return getFormatY(value);
            }
        });

        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);

        /*Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        */

    }


    @SuppressLint("StaticFieldLeak")
    @Override
    public void getDataApps(String app, long from, long to) {

        this.from = from;
        this.to = to;

        new CharTask(progress){

            @Override
            protected Boolean doInBackground(Void... voids) {
                super.doInBackground(voids);
                return isUseStaticApps(app);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {

                if (aBoolean){
                    List<ILineDataSet> barDataSets = new ArrayList<>();
                    for (TempDataSet tempDataSet : tempDataSets){

                        List<Entry> barEntries = new ArrayList<>();
                        for (TempApp tempApp : tempDataSet.getList()){
                            barEntries.add(new Entry(tempApp.getX(), tempApp.getY()));
                        }

                        LineDataSet barDataSet = new LineDataSet(barEntries, tempDataSet.getAppName());
                        barDataSet.setColor(tempDataSet.getColor());
                        barDataSet.setLineWidth(2.5f);
                        barDataSet.setCircleRadius(4f);
                        barDataSet.setColor(tempDataSet.getColor());
                        barDataSet.setCircleColor(tempDataSet.getColor());

                        barDataSets.add(barDataSet);
                    }


                    LineData data = new LineData(barDataSets);

                    data.setValueFormatter(new ValueFormatter() {
                        @Override
                        public String getFormattedValue(float value) {
                            return getFormatY(value);
                        }
                    });

                    chart.setData(data);

                    chart.invalidate();

                    listener.onCharResult(TimeUtil.convertMillis(d), usedAppList);
                }else {
                    listener.onCharResult("00:00:00", usedAppList);
                }

                super.onPostExecute(aBoolean);
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);




    }



    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
