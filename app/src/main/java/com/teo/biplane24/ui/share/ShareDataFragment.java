package com.teo.biplane24.ui.share;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Share;
import com.teo.biplane24.ui.share.adapters.ShareAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareDataFragment extends Fragment {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.button_add)
    Button buttonAdd;

    private ShareAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_share_data, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ShareAdapter(getContext());
        recyclerView.setAdapter(adapter);

        ClipboardManager clipboardManager = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                Toast.makeText(getContext(), "Скопировано!", Toast.LENGTH_SHORT).show();
            }
        });
        adapter.setListener(new ShareAdapter.ShareListener() {
            @Override
            public void onCopy(Share share) {

                clipboardManager.setPrimaryClip(ClipData.newPlainText("text", share.getHref()));

            }

            @Override
            public void onDelete(Share share) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.app_name)
                        .setMessage("Удалить?")
                        .setPositiveButton("ок", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Call<String> call = App.getRetrofitApi().deleteShare(App.getInstance().getUser().getToken(), share.getId());
                                call.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        if (response != null && response.body() != null){
                                            adapter.getList().remove(share);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {

                                    }
                                });
                            }
                        })
                        .setNegativeButton("Отмена", null)
                        .show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Call<List<Share>> call = App.getRetrofitApi().getShareList(App.getInstance().getUser().getToken());
        call.enqueue(new Callback<List<Share>>() {
            @Override
            public void onResponse(Call<List<Share>> call, Response<List<Share>> response) {
                if (response != null && response.body() != null){
                    if (getActivity() != null && adapter != null) {
                        adapter.setList(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Share>> call, Throwable t) {

            }
        });

    }

    @OnClick(R.id.button_add)
    public void add(){
        startActivity(new Intent(getContext(), AddShareActivity.class));
    }

}