package com.teo.biplane24.ui.report.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.UsedApp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportUserAdapter extends RecyclerView.Adapter<ReportUserAdapter.ViewHolder>{

    public List<UsedApp> getList() {
        return list;
    }

    public void setList(List<UsedApp> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<UsedApp> list;
    public ReportUserAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    private Context context;

    public ReportUserAdapter.AppClickListener getListener() {
        return listener;
    }

    public void setListener(ReportUserAdapter.AppClickListener listener) {
        this.listener = listener;
    }

    private ReportUserAdapter.AppClickListener listener;
    public interface AppClickListener{
        void onAppClick(UsedApp item);
    }

    @NonNull
    @Override
    public ReportUserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_app, parent, false);
        return new ReportUserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportUserAdapter.ViewHolder holder, int position) {

        final UsedApp item = list.get(position);

        holder.textName.setText(item.getName());
        holder.textTime.setText(TimeUtil.convertMillis(item.getUsedTime()));
        holder.imageView.setImageResource(R.drawable.ic_perm_identity_24px);
        holder.imageColor.setColorFilter(item.getColor());
        holder.imageView.setColorFilter(item.getColor());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAppClick(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_image)
        ImageView imageView;
        @BindView(R.id.item_color)
        ImageView imageColor;
        @BindView(R.id.item_name)
        TextView textName;
        @BindView(R.id.item_time)
        TextView textTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}