package com.teo.biplane24.ui.report.users;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.teo.biplane24.App;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Tochka;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.BaseCharHelper;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class UsersBaseChartHelper extends BaseCharHelper implements OnChartValueSelectedListener {


    public Map<Integer, ArrayList<TempApp>> map;


    public UsersBaseChartHelper(Context context, OnCharResultListener listener) {
        this.context = context;
        this.listener = listener;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public void getDataUsers(int app, long from, long to) {

    }

    public boolean isUseStaticUsers(int appSelect) {
        map = new HashMap<>();
        List<Report> all = App.getInstance().getDatabase().getUserDao().getReports();

        usedAppList = new ArrayList<>();

        if (all.size() > 0) {

            int i = 0;

            periods = new ArrayList<>();
            while (from < to) {
                long p = from + period;

                List<Report> list = App.getInstance().getDatabase().getUserDao().getReports(from, p);
                if (list != null && list.size() > 0) {
                    periods.add(p - period);
                    Map<Integer, List<Report>> mapTime = new HashMap<>();

                    for (Report appTime : list) {

                        Round round = App.getInstance().getDatabase().getUserDao().getRound(appTime.getUser_id());

                        if (round != null){

                            List<Report> timeList = mapTime.get(round.getId());

                            if (timeList == null) {
                                timeList = new ArrayList<>();

                            }
                            timeList.add(appTime);

                            mapTime.put(round.getId(), timeList);
                        }

                    }

                    Iterator<Map.Entry<Integer, List<Report>>> it = mapTime.entrySet().iterator();

                    if (appSelect == 0){
                        while (it.hasNext()) {

                            Map.Entry<Integer, List<Report>> pair = it.next();
                            int app = pair.getKey();

                            List<Report> timeList = pair.getValue();

                            long durationTimes = 0;
                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }

                            long appD = 0;

                            if (durationTimes > 0) {

                                appD += durationTimes;

                                d += durationTimes;

                                ArrayList<TempApp> mapList = map.get(app);
                                if (mapList == null) {
                                    mapList = new ArrayList<>();
                                }

                                mapList.add(new TempApp(i, durationTimes, appD));

                                map.put(app, mapList);
                            }
                        }
                    }else {

                        List<Report> timeList = mapTime.get(appSelect);

                        long durationTimes = 0;

                        if (timeList != null){
                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }
                        }

                        long appD = 0;

                        if (durationTimes > 0) {

                            appD += durationTimes;

                            d += durationTimes;

                            ArrayList<TempApp> mapList = map.get(appSelect);
                            if (mapList == null) {
                                mapList = new ArrayList<>();
                            }

                            mapList.add(new TempApp(i, durationTimes, appD));

                            map.put(appSelect, mapList);


                        }
                    }


                    i++;
                }

                from += period;
            }

            tempDataSets = new ArrayList<>();

            Iterator<Map.Entry<Integer, ArrayList<TempApp>>> it = map.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry<Integer, ArrayList<TempApp>> pair = it.next();

                ArrayList<TempApp> entries = pair.getValue();
                int key = pair.getKey();

                long duration = 0;

                for (TempApp entry : entries) {
                    duration += entry.getDuration();
                }

                Round round = App.getInstance().getDatabase().getUserDao().getRoundId(key);

                if (round != null){
                    UsedApp usedApp = new UsedApp();
                    usedApp.setUser_id(key);
                    usedApp.setColor(App.getInstance().getColorApp(key + "user"));
                    usedApp.setUsedTime(duration);
                    usedApp.setName(round.getName());
                    usedAppList.add(usedApp);
                }
            }

            usedAppList = sortList(usedAppList);

            if (usedAppList.size() > maxLegendCount) {
                for (int i1 = maxLegendCount; i1 < usedAppList.size(); i1++) {
                    UsedApp app = usedAppList.get(i1);
                    map.remove(app.getUser_id());
                }

            }

            it = map.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<Integer, ArrayList<TempApp>> pair = it.next();

                ArrayList<TempApp> entries = pair.getValue();
                int key = pair.getKey();

                Round round = App.getInstance().getDatabase().getUserDao().getRoundId(key);

                if (round != null) {

                    TempDataSet set = new TempDataSet(entries, round.getName());

                    set.setColor(App.getInstance().getColorApp(key + "user"));

                    tempDataSets.add(set);
                }
            }

            if (tempDataSets.size() > 0) {

                return true;
            }
        }

        return false;
    }



}
