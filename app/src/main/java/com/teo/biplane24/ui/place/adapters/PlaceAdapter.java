package com.teo.biplane24.ui.place.adapters;

import android.content.Context;
import android.graphics.Point;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.R;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Round;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder>{

    public List<Place> getList() {
        return list;
    }

    public void setList(List<Place> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<Place> list;

    public PlaceAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public PlaceListener getListener() {
        return listener;
    }

    public void setListener(PlaceListener listener) {
        this.listener = listener;
    }

    private PlaceListener listener;

    public interface PlaceListener {
        void onDelete(Place place);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_place, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Place item = list.get(position);
        holder.textName.setText(item.getName());
        holder.textRadius.setText("Радиус метров: " + item.getRadius());
        holder.adapter = new PointAdapter(context);
        //holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2, RecyclerView.VERTICAL, false));
        holder.recyclerView.setAdapter(holder.adapter);
        holder.adapter.setListener(new PointAdapter.PlaceListener() {
            @Override
            public void onClick(String place) {

            }
        });
        holder.setList(item);
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDelete(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_name)
        TextView textName;
        @BindView(R.id.item_radius)
        TextView textRadius;
        @BindView(R.id.recycler_view)
        RecyclerView recyclerView;

        @BindView(R.id.item_delete)
        ImageView imageDelete;

        public PointAdapter adapter;

        public void setList(Place item) {
            if (list == null){
                list = new ArrayList<>();

                list.add(item.getPosition1());

                if (item.getPosition2()!= null){
                    list.add(item.getPosition2());
                }
                if (item.getPosition3()!= null){
                    list.add(item.getPosition3());
                }
                if (item.getPosition4() != null){
                    list.add(item.getPosition4());
                }
            }
            adapter.setList(list);
        }

        private List<String> list;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
