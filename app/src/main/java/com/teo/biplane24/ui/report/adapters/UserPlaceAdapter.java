package com.teo.biplane24.ui.report.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.UsedApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserPlaceAdapter extends RecyclerView.Adapter<UserPlaceAdapter.ViewHolder>{

    public List<UsedApp> getList() {
        return list;
    }

    public void setList(List<UsedApp> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<UsedApp> list;
    public UserPlaceAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    private Context context;



    @NonNull
    @Override
    public UserPlaceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_user_place, parent, false);
        return new UserPlaceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserPlaceAdapter.ViewHolder holder, int position) {

        final UsedApp item = list.get(position);

        holder.textName.setText(item.getName());
        holder.textFrom.setText(getFormatDate(item.getTimeStart()));
        holder.textTo.setText(getFormatDate(item.getTimeEnd()));
        holder.textTime.setText(TimeUtil.convertMillis(item.getUsedTime()));

    }

    public String getFormatDate(long date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String fo =  format.format(new Date(date));

        return fo;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_name)
        TextView textName;
        @BindView(R.id.item_time)
        TextView textTime;
        @BindView(R.id.item_from)
        TextView textFrom;
        @BindView(R.id.item_to)
        TextView textTo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}