package com.teo.biplane24.ui.round.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoundAdapter extends RecyclerView.Adapter<RoundAdapter.ViewHolder>{

    public List<Round> getList() {
        return list;
    }

    public void setList(List<Round> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<Round> list;

    public RoundAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public RoundListener getListener() {
        return listener;
    }

    public void setListener(RoundListener listener) {
        this.listener = listener;
    }

    private RoundListener listener;

    public interface RoundListener {
        void onDelete(Round share);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_round, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Round item = list.get(position);
        holder.textHref.setText(item.getHref());
        Status status = App.getInstance().getDatabase().getUserDao().getStatus(item.getStatus_id());
        if (status != null) {
            holder.textStatus.setText(status.getName());
        }
        Group group = App.getInstance().getDatabase().getUserDao().getGroup(item.getGroup_id());
        if (group != null) {
            holder.textGroup.setText(group.getName());
        }
        holder.textName.setText(item.getName());
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDelete(item);
            }
        });

    }

    /*
    [{"id":1,"user_id":3,"name":"статус 1"},
    {"id":8,"user_id":3,"name":"статус 2"},
    {"id":9,"user_id":3,"name":"статус 3"},
    {"id":10,"user_id":3,"name":"статус 5"},
    {"id":11,"user_id":3,"name":"статус 4"}]

[{"id":20,"user_id":3,"href_user_id":7,"href":"http://biplane24.teo-crm.com/EwjGNrcN","name":"тест 1","status_id":8,"group_id":12},
{"id":21,"user_id":3,"href_user_id":21,"href":"http://biplane24.teo-crm.com/BqbbMdZu","name":"тест 2","status_id":9,"group_id":13},
{"id":24,"user_id":3,"href_user_id":22,"href":"http://biplane24.teo-crm.com/0B7rdR3a","name":"тест 3","status_id":11,"group_id":14}]

     */

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_href)
        TextView textHref;
        @BindView(R.id.item_name)
        TextView textName;
        @BindView(R.id.item_status)
        TextView textStatus;
        @BindView(R.id.item_group)
        TextView textGroup;
        @BindView(R.id.item_delete)
        ImageView imageDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

