package com.teo.biplane24.ui.report.apps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.ui.MyMarkerView;
import com.teo.biplane24.ui.PieMarkerView;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.util.ArrayList;

public class AppsPieHelper extends AppsBaseChartHelper {

    private PieChart chart;

    public AppsPieHelper(Context context, LinearLayout root, OnCharResultListener listener) {
        super(context, listener);
        chart = new PieChart(context);
        chart.setNoDataText(context.getResources().getString(R.string.char_empty));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        root.addView(chart, params);

        PieMarkerView mv = new PieMarkerView(context, R.layout.custom_marker_view);
        mv.setChartView(chart);
        chart.setMarker(mv);

        chart.setDrawEntryLabels(false);
        chart.setUsePercentValues(true);
        //chart.setDrawEntryLabels(false);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(padintPie, padintPie, padintPie, padintPie);

        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(50f);
        chart.setTransparentCircleRadius(52f);

        //chart.setDrawCenterText(true);

        chart.setRotationAngle(90);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);


        chart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(false);

        // entry label styling
        chart.setEntryLabelColor(Color.BLACK);
        chart.setEntryLabelTextSize(12f);


    }



    @SuppressLint("StaticFieldLeak")
    @Override
    public void getDataApps(String app, long from, long to) {

        this.from = from;
        this.to = to;

        new CharTask(progress){
            @Override
            protected Boolean doInBackground(Void... voids) {
                super.doInBackground(voids);
                return isUseStaticApps(app);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {

                if(aBoolean){


                    ArrayList<PieEntry> entries = new ArrayList<>();
                    ArrayList<Integer> colors = new ArrayList<>();
                    for (TempDataSet tempDataSet : tempDataSets){

                        float value = 0;
                        long duration = 0;

                        for (TempApp tempApp : tempDataSet.getList()){
                            value += tempApp.getY();
                            duration += tempApp.getY();
                        }

                        int progress = (int) ((duration * 100) / d);
                        Log.e("tr", "percent " + progress);
                        if (progress >= 2){
                            entries.add(new PieEntry(value, tempDataSet.getAppName()));
                            colors.add(tempDataSet.getColor());
                        }

                    }

                    PieDataSet dataSet = new PieDataSet(entries, "Usage App");
                    dataSet.setSliceSpace(1f);
                    dataSet.setSelectionShift(5f);
                    dataSet.setAutomaticallyDisableSliceSpacing(true);

                    dataSet.setValueLineColor(Color.BLACK);

                    dataSet.setColors(colors);
                    //dataSet.setSelectionShift(0f);


                    dataSet.setValueLinePart1OffsetPercentage(90.f);
                    dataSet.setValueLinePart1Length(0.4f);
                    dataSet.setValueLinePart2Length(0.1f);
                    dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
                    //dataSet.setUsingSliceColorAsValueLineColor(true);

                    //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
                    dataSet.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);

                    PieData data = new PieData(dataSet);
                    data.setValueFormatter(new PercentFormatter());
                    data.setValueTextSize(10f);
                    data.setValueTextColor(Color.BLACK);

                    chart.setData(data);

                    // undo all highlights
                    chart.highlightValues(null);

                    chart.invalidate();
                    listener.onCharResult(TimeUtil.convertMillis(d), usedAppList);
                }else {
                    listener.onCharResult("00:00:00", usedAppList);
                }

                super.onPostExecute(aBoolean);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
