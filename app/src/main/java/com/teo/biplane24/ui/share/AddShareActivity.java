package com.teo.biplane24.ui.share;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.teo.biplane24.App;
import com.teo.biplane24.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddShareActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.button_save)
    Button buttonSave;
    @BindView(R.id.period_time_from)
    TextView periodTimeFrom;
    @BindView(R.id.period_time_to)
    TextView periodTimeTo;
    @BindView(R.id.item_comment)
    EditText editComment;

    @BindView(R.id.check_pn)
    CheckBox check1;
    @BindView(R.id.check_vt)
    CheckBox check2;
    @BindView(R.id.check_cr)
    CheckBox check3;
    @BindView(R.id.check_sht)
    CheckBox check4;
    @BindView(R.id.check_pt)
    CheckBox check5;
    @BindView(R.id.check_sb)
    CheckBox check6;
    @BindView(R.id.check_vs)
    CheckBox check7;

    @BindView(R.id.layout_checks)
    LinearLayout layoutChecks;

    private Calendar cFrom;
    private Calendar cTo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_share);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        cFrom = Calendar.getInstance();
        cTo = Calendar.getInstance();

        periodTimeFrom.setText(getFormatTime(cFrom.getTimeInMillis()));
        periodTimeTo.setText(getFormatTime(cTo.getTimeInMillis()));


    }

    private String getFormatTime(long period){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(new Date(period));

    }

    @OnClick(R.id.button_save)
    public void save(){
        String text = editComment.getText().toString();
        String from = periodTimeFrom.getText().toString();
        String to = periodTimeTo.getText().toString();
        if (!TextUtils.isEmpty(text)){
            buttonSave.setEnabled(false);
            StringBuilder builder = new StringBuilder();

            int count = layoutChecks.getChildCount();
            List<Integer> days = new ArrayList<>();
            for (int i = 0; i < count; i++){
                CheckBox checkBox = (CheckBox) layoutChecks.getChildAt(i);
                if (checkBox.isChecked()){
                    days.add(i + 1);
                }
            }

            for (int i = 0; i < days.size(); i++){
                int day = days.get(i);
                builder.append(day);
                if (i < days.size() - 1){
                    builder.append(",");
                }
            }

            Call<String> call = App.getRetrofitApi().createShare(App.getInstance().getUser().getToken(), builder.toString(), from, to, text);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response!= null && response.body() != null){
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    buttonSave.setEnabled(true);
                }
            });
        }
    }

    @OnClick(R.id.period_time_from)
    public void showFrom(){
        showFromTimeDialog();
    }

    @OnClick(R.id.period_time_to)
    public void showTo(){
        showToTimeDialog();
    }

    private void showFromTimeDialog(){

        int mHour = cFrom.get(Calendar.HOUR_OF_DAY);
        int mMinute = cFrom.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


                        cFrom.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        cFrom.set(Calendar.MINUTE, minute);

                        periodTimeFrom.setText(hourOfDay + ":"+ minute);

                    }
                }, mHour, mMinute, true);

        timePickerDialog.show();
    }

    private void showToTimeDialog(){

        int mHour = cTo.get(Calendar.HOUR_OF_DAY);
        int mMinute = cTo.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        cTo.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        cTo.set(Calendar.MINUTE, minute);

                        periodTimeTo.setText(hourOfDay + ":"+ minute);

                    }
                }, mHour, mMinute, true);

        timePickerDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
