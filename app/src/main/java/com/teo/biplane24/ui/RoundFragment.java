package com.teo.biplane24.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoundFragment extends Fragment {

    @BindView(R.id.item_spinner)
    Spinner spinner;
    @BindView(R.id.item_add)
    ImageView buttonAdd;
    @BindView(R.id.layout_select)
    LinearLayout layoutSelect;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragent_group, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private List<Round> list;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        buttonAdd.setVisibility(View.GONE);
        list = new ArrayList<>();
        Round status = new Round();
        status.setName("Все окружения");
        status.setId(0);
        list.add(status);

        List<Round> list1 = App.getInstance().getDatabase().getUserDao().getRounds();
        //list.addAll(list1);
        for (Round round : list1){
            if (round.getHref_user_id() == App.getInstance().getUser().getId() && round.getId() == App.getInstance().getUser().getId()){
                list.add(round);
            }else
            if (round.getHref_user_id() != App.getInstance().getUser().getId()){
                list.add(round);
            }
        }

        layoutSelect.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        ArrayAdapter<Round> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(arrayAdapter);
        //spinner.setSelection(preferences.getInt("round_selected", 0));
        int select = App.getInstance().getFilter().getRound();
        for (int i = 0; i < list.size(); i++){
            Round item = list.get(i);
            if (item.getId() == select){
                select = i;
            }
        }
        if (select < list.size()) {
            spinner.setSelection(select);
        }

    }

    public Round getSelect(){

        return (Round) spinner.getSelectedItem();

    }

}