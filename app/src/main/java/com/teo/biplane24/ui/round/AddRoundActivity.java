package com.teo.biplane24.ui.round;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.ui.GroupFragment;
import com.teo.biplane24.ui.StatusFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRoundActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.edit_href)
    EditText editHref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_round);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @OnClick(R.id.button_save)
    public void save(){
        String name = editName.getText().toString();
        String href = editHref.getText().toString();
        Group group = ((GroupFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_group)).getSelect();
        Status status = ((StatusFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_status)).getSelect();
        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(href) && status != null && group != null){
            Call<Round> call = App.getRetrofitApi().createRound(
                    App.getInstance().getUser().getToken(),  status.getId(), group.getId(), href, name);
            call.enqueue(new Callback<Round>() {
                @Override
                public void onResponse(Call<Round> call, Response<Round> response) {
                    if (response != null && response.body() != null){
                        App.getInstance().getDatabase().getUserDao().insertRound(response.body());
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<Round> call, Throwable t) {

                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
