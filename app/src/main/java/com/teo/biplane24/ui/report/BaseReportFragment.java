package com.teo.biplane24.ui.report;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.teo.biplane24.App;
import com.teo.biplane24.model.Filter;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Tochka;
import com.teo.biplane24.ui.report.places.ReportPlacesFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseReportFragment extends Fragment {

    private BroadcastReceiver receiver;

    public abstract void getData();

    public abstract void getLocalData();


    Call<List<Report>> call;


    public Filter filter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        filter = App.getInstance().getFilter();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getData();
            }
        };

        getActivity().registerReceiver(receiver, new IntentFilter("update.filter"));
    }

    @Override
    public void onDestroy() {
        if (call != null && !call.isCanceled()) {
            Log.e("tr", "cancel call");
            call.cancel();
        }
        getActivity().unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {

        }
    }

    public String getFormatDate(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String fo = format.format(new Date(date));

        return fo;
    }

    public String getFormatDateUTC(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String fo = format.format(new Date(date));

        return fo;
    }

    public void getReports(final OnReportListener listener) {


        Map<String, Integer> map = new HashMap<>();

        if (filter.getGroup() > 0) {
            map.put("group_id", filter.getGroup());
        }

        if (filter.getStatus() > 0) {
            map.put("status_id", filter.getStatus());
        }

        if (filter.getPlace() > 0) {
            map.put("place_id", filter.getPlace());
        }

        if (filter.getRound() > 0) {
            if (filter.getRound() == App.getInstance().getUser().getId()) {
                map.put("filterMe", 1);
            } else {
                map.put("round_id", filter.getRound());
                map.put("filterMe", 0);
            }
        } else {
            map.put("filterMe", 0);
        }

        call = App.getRetrofitApi().getReports(App.getInstance().getUser().getToken(), getFormatDateUTC(filter.getFrom()), getFormatDateUTC(filter.getTo()), map);

        if (this instanceof ReportPlacesFragment) {
            call = App.getRetrofitApi().getLocations(App.getInstance().getUser().getToken(), getFormatDateUTC(filter.getFrom()), getFormatDateUTC(filter.getTo()), map);
        }

        call.enqueue(new Callback<List<Report>>() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onResponse(Call<List<Report>> call, Response<List<Report>> response) {
                if (response != null && response.body() != null) {
                    if (getActivity() != null) {

                        new ReportTack() {
                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                listener.onSuccess();
                            }
                        }.execute(response.body());

                    } else {
                        listener.onFailure();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Report>> call, Throwable t) {
                listener.onFailure();
            }
        });

    }

    private List<Report> reportList;


    public class ReportTack extends AsyncTask<List<Report>, Void, Void> {
        @Override
        protected Void doInBackground(List<Report>... lists) {
            App.getInstance().getDatabase().getUserDao().deleteReports(App.getInstance().getDatabase().getUserDao().getReports());

            List<Place> places = App.getInstance().getDatabase().getUserDao().getPlaces();

            reportList = new ArrayList<>();

            App.getInstance().getDatabase().getUserDao().insertReports(lists[0]);

            reportList.addAll(App.getInstance().getDatabase().getUserDao().getReports());

            App.getInstance().getDatabase().getUserDao().deleteReports(App.getInstance().getDatabase().getUserDao().getReports());

            Map<String, List<Report>> map = new HashMap<>();

            for (Report report : reportList) {

                List<Report> list = map.get(String.valueOf(report.getUser_id()));
                if (list == null) {
                    list = new ArrayList<>();
                }

                if (BaseReportFragment.this instanceof ReportPlacesFragment) {
                    if (!TextUtils.isEmpty(report.getCoords()) && report.getPlace_id() == 0) {
                        String[] coordsApp = report.getCoords().split(",");
                        Tochka app = new Tochka(Double.parseDouble(coordsApp[0]), Double.parseDouble(coordsApp[1]));

                        if (filter.getPlace() > 0) {
                            Place place = App.getInstance().getDatabase().getUserDao().getPlaceId(filter.getPlace());
                            if (!TextUtils.isEmpty(place.getPosition1())) {
                                report.setPlace_id(getPlaceId(place, app));
                                if (report.getPlace_id() > 0) {
                                }
                            }

                            if (!TextUtils.isEmpty(place.getPosition2())) {
                                report.setPlace_id(getPlaceId(place, app));
                                if (report.getPlace_id() > 0) {

                                }
                            }

                            if (!TextUtils.isEmpty(place.getPosition3())) {
                                report.setPlace_id(getPlaceId(place, app));
                                if (report.getPlace_id() > 0) {

                                }
                            }

                            if (!TextUtils.isEmpty(place.getPosition4())) {
                                report.setPlace_id(getPlaceId(place, app));
                                if (report.getPlace_id() > 0) {

                                }
                            }
                        } else {
                            for (Place place : places) {
                                if (!TextUtils.isEmpty(place.getPosition1())) {
                                    report.setPlace_id(getPlaceId(place, app));
                                    if (report.getPlace_id() > 0) {
                                        break;
                                    }
                                }

                                if (!TextUtils.isEmpty(place.getPosition2())) {
                                    report.setPlace_id(getPlaceId(place, app));
                                    if (report.getPlace_id() > 0) {
                                        break;
                                    }
                                }

                                if (!TextUtils.isEmpty(place.getPosition3())) {
                                    report.setPlace_id(getPlaceId(place, app));
                                    if (report.getPlace_id() > 0) {
                                        break;
                                    }
                                }

                                if (!TextUtils.isEmpty(place.getPosition4())) {
                                    report.setPlace_id(getPlaceId(place, app));
                                    if (report.getPlace_id() > 0) {
                                        break;
                                    }
                                }
                            }
                        }

                    }
                }

                list.add(report);

                map.put(String.valueOf(report.getUser_id()), list);
            }

            reportList.clear();


            Iterator<Map.Entry<String, List<Report>>> it = map.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, List<Report>> pair = it.next();

                List<Report> list = pair.getValue();


                for (int i = 0; i < list.size(); i++) {
                    Report report = list.get(i);
                    report.setTime_start(report.getTime_start() * 1000);
                    report.setTime_work(report.getTime_work() * 1000);
                }

                if (BaseReportFragment.this instanceof ReportPlacesFragment) {


                    Report oldReport = null;

                    for (int i = 0; i < list.size(); i++) {

                        Report report = list.get(i);


                        if (report.getPlace_id() == 0) {
                            for (Place place : places) {
                                report.setPlace_id(searchPlaceInNext(place, list, i));
                                if (report.getPlace_id() > 0) {

                                    break;
                                }
                            }
                        }

                        //if (filter.getPlace() == 0) {
                        if (oldReport == null) {
                            oldReport = report;
                        }

                        long start_old = oldReport.getTime_start() + oldReport.getTime_work();
                        long start = report.getTime_start();
                        long offset = (start - start_old) / 60000;

                        if (offset > 0 && offset < 5) {

                            for (int t = 0; t < offset; t++) {
                                long time = start_old + TimeUnit.MINUTES.toMillis(t);
                                Report fakeReport = new Report();
                                fakeReport.setPlace_id(report.getPlace_id());
                                fakeReport.setTime_work(60000);
                                fakeReport.setTime_start(time);
                                fakeReport.setCoords(report.getCoords());
                                fakeReport.setUser_id(report.getUser_id());
                                fakeReport.setDatetime(report.getDatetime());

                                reportList.add(fakeReport);
                            }

                        }

                        oldReport = report;

                        //}

                        reportList.add(report);
                    }


                }

                reportList.addAll(list);

            }

            App.getInstance().getDatabase().getUserDao().insertReports(reportList);
            return null;
        }
    }

    private int searchPlaceInNext(Place place, List<Report> list, int position) {
        String[] coords = place.getPosition1().split(",");
        Tochka coordPlace = new Tochka(Double.parseDouble(coords[0]), Double.parseDouble(coords[1]));

        int add = 2;
        int max = position + add;


        if (list.size() > position + add) {
            for (int i = max; i > position; i--) {
                Report report = list.get(i);
                String[] coordsApp = report.getCoords().split(",");
                Tochka app = new Tochka(Double.parseDouble(coordsApp[0]), Double.parseDouble(coordsApp[1]));
                float distance = checkDistance(coordPlace, app);
                int radius = place.getRadius();
                if (distance <= radius) {
                    return place.getId();
                }
            }
        }

        return 0;
    }


    private int getPlaceId(Place place, Tochka app) {
        String[] coords = place.getPosition1().split(",");
        Tochka tochka = new Tochka(Double.parseDouble(coords[0]), Double.parseDouble(coords[1]));


        float distance = checkDistance(tochka, app);
        int radius = place.getRadius();
        if (distance <= radius) {
            return place.getId();
        }

        return 0;

    }


    public float checkDistance(Tochka app, Tochka point) {

        Location locationA = new Location("point A");

        locationA.setLatitude(app.getLat());
        locationA.setLongitude(app.getLng());

        Location locationB = new Location("point B");

        locationB.setLatitude(point.getLat());
        locationB.setLongitude(point.getLng());

        float distance = locationA.distanceTo(locationB);


        return distance;
    }


    public interface OnReportListener {
        void onSuccess();

        void onFailure();
    }


}
