package com.teo.biplane24.ui;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Status;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatusFragment extends Fragment {

    @BindView(R.id.item_spinner)
    Spinner spinner;
    @BindView(R.id.item_add)
    ImageView buttonAdd;
    @BindView(R.id.layout_select)
    LinearLayout layoutSelect;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragent_group, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private List<Status> list;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        list = new ArrayList<>();
        Status status = new Status();
        status.setName("Все статусы");
        status.setId(0);
        list.add(status);
        list.addAll(App.getInstance().getDatabase().getUserDao().getStatuses());

        layoutSelect.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        ArrayAdapter<Status> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(arrayAdapter);

        //spinner.setSelection(preferences.getInt("status_selected", 0));
        int select = App.getInstance().getFilter().getStatus();
        for (int i = 0; i < list.size(); i++){
            Status item = list.get(i);
            if (item.getId() == select){
                select = i;
            }
        }
        if (select < list.size()) {
            spinner.setSelection(select);
        }
    }

    public Status getSelect(){

        return (Status) spinner.getSelectedItem();


    }

    @OnClick(R.id.item_add)
    public void add(){
        EditText editText = new EditText(getContext());
        new AlertDialog.Builder(getContext())
                .setTitle("Создать статус")
                .setView(editText)
                .setPositiveButton("Создать", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String text = editText.getText().toString();
                        if (!TextUtils.isEmpty(text)){

                            layoutSelect.setVisibility(View.GONE);
                            progressBar.setVisibility(View.VISIBLE);
                            Call<Status> call = App.getRetrofitApi().createStatus(App.getInstance().getUser().getToken(), text);
                            call.enqueue(new Callback<Status>() {
                                @Override
                                public void onResponse(Call<Status> call, Response<Status> response) {
                                    if (response != null && response.body() != null){
                                        layoutSelect.setVisibility(View.VISIBLE);
                                        progressBar.setVisibility(View.GONE);
                                        list.add(response.body());
                                        App.getInstance().getDatabase().getUserDao().insertStatus(response.body());

                                        ArrayAdapter<Status> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
                                        spinner.setAdapter(arrayAdapter);
                                    }
                                }

                                @Override
                                public void onFailure(Call<Status> call, Throwable t) {

                                }
                            });
                        }
                    }
                })
                .show();
    }
}
