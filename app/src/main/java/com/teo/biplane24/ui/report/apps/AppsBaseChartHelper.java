package com.teo.biplane24.ui.report.apps;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.teo.biplane24.App;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Tochka;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.BaseCharHelper;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class AppsBaseChartHelper extends BaseCharHelper implements OnChartValueSelectedListener {

    public Map<String, ArrayList<TempApp>> map;


    public int periodId;

    public AppsBaseChartHelper(Context context, OnCharResultListener listener) {
        this.context = context;
        this.listener = listener;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }




    public void getDataApps(String app, long from, long to){

    }

    public void getDataUsers(String app, long from, long to){

    }

    public boolean isUseStaticApps(String appSelect) {
        map = new HashMap<>();
        List<Report> all = App.getInstance().getDatabase().getUserDao().getReports();

        usedAppList = new ArrayList<>();

        if (all.size() > 0) {

            int i = 0;

            Log.e("tr", "appSelect " + appSelect);

            periods = new ArrayList<>();
            while (from < to) {
                long p = from + period;

                List<Report> list;
                if (appSelect == null){
                    list = App.getInstance().getDatabase().getUserDao().getReports(from, p);
                }else {
                    list = App.getInstance().getDatabase().getUserDao().getReports(from, p, appSelect);
                }

                if (list != null && list.size() > 0) {
                    periods.add(p - period);
                    Map<String, List<Report>> mapTime = new HashMap<>();


                    for (Report appTime : list) {

                        List<Report> timeList = mapTime.get(appTime.getApp_id());
                        if (timeList == null) {
                            timeList = new ArrayList<>();
                        }

                        timeList.add(appTime);

                        mapTime.put(appTime.getApp_id(), timeList);
                    }

                    Iterator<Map.Entry<String, List<Report>>> it = mapTime.entrySet().iterator();

                    //if (appSelect == null) {
                        while (it.hasNext()) {

                            Map.Entry<String, List<Report>> pair = it.next();
                            String app = pair.getKey();

                            List<Report> timeList = pair.getValue();

                            long durationTimes = 0;
                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }

                            long appD = 0;

                            if (durationTimes > 0) {

                                appD += durationTimes;


                                d += durationTimes;

                                ArrayList<TempApp> mapList = map.get(app);
                                if (mapList == null) {
                                    mapList = new ArrayList<>();
                                }

                                mapList.add(new TempApp(i, durationTimes, appD));

                                map.put(app, mapList);


                            }
                        }
                    /*}else {
                        List<Report> timeList = mapTime.get(appSelect);

                        long durationTimes = 0;
                        if (timeList != null){
                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }
                        }


                        if (durationTimes > 0) {

                            d += durationTimes;

                            ArrayList<TempApp> mapList = map.get(appSelect);
                            if (mapList == null) {
                                mapList = new ArrayList<>();
                            }

                            mapList.add(new TempApp(i, durationTimes, durationTimes));

                            map.put(appSelect, mapList);

                        }
                    }*/

                    i++;
                }


                from += period;
            }

            tempDataSets = new ArrayList<>();

            Iterator<Map.Entry<String, ArrayList<TempApp>>> it = map.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry<String, ArrayList<TempApp>> pair = it.next();

                ArrayList<TempApp> entries = pair.getValue();
                String key = pair.getKey();

                long duration = 0;

                for (TempApp entry : entries) {
                    duration += entry.getDuration();
                }



                UsedApp usedApp = new UsedApp();
                usedApp.setApp(key);
                usedApp.setColor(App.getInstance().getColorApp(key));
                usedApp.setUsedTime(duration);

                usedApp.setName(getAppNameFromPkgName(context, key));
                usedAppList.add(usedApp);
            }

            usedAppList = sortList(usedAppList);


            if (usedAppList.size() > maxLegendCount) {
                for (int i1 = maxLegendCount; i1 < usedAppList.size(); i1++) {
                    UsedApp app = usedAppList.get(i1);
                    map.remove(app.getApp());
                }

            }

            it = map.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, ArrayList<TempApp>> pair = it.next();

                ArrayList<TempApp> entries = pair.getValue();
                String key = pair.getKey();

                TempDataSet set = new TempDataSet(entries, getAppNameFromPkgName(context, key));
                set.setColor(App.getInstance().getColorApp(key));

                tempDataSets.add(set);
            }

            return true;

        }



        return false;
    }


    public String getAppNameFromPkgName(Context context, String Packagename) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo info = packageManager.getApplicationInfo(Packagename, PackageManager.GET_META_DATA);
            String appName = (String) packageManager.getApplicationLabel(info);
            return appName;
        } catch (PackageManager.NameNotFoundException e) {
            //e.printStackTrace();
            return Packagename;
        }
    }

}
