package com.teo.biplane24.ui.report.apps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.teo.biplane24.FilterActivity;
import com.teo.biplane24.R;
import com.teo.biplane24.model.UsedApp;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppReportActivity extends AppCompatActivity implements AppsBaseChartHelper.OnCharResultListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.item_sum)
    TextView textViewSum;
    @BindView(R.id.chart_inflater)
    LinearLayout layoutChar;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;

    private AppsBaseChartHelper charHelper;
    private SharedPreferences preferences;

    private int type;

    private UsedApp app;

    private long from;
    private long to;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_report);
        ButterKnife.bind(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        from = getIntent().getLongExtra("from", 0);
        to = getIntent().getLongExtra("to", 0);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        app = (UsedApp) getIntent().getSerializableExtra("app");

        actionBar.setTitle(app.getName());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);

        return true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    public void getData() {

        progressBar.setVisibility(View.VISIBLE);

        int type = preferences.getInt("type", 1);
        if (this.type != type){
            this.type = type;

            layoutChar.removeAllViews();

            switch (type){
                case 1:
                    charHelper = new AppsStackedHelper(this, layoutChar,this);
                    break;
                case 2:
                    charHelper = new AppsLineHelper(this, layoutChar, this);
                    break;
                case 3:
                    charHelper = new AppsPieHelper(this, layoutChar, this);
                    break;
            }
        }

        charHelper.setProgress(progressBar);
        charHelper.getDataApps(app.getApp(), from, to);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_filter:
                Intent intent = new Intent(this, FilterActivity.class);
                intent.putExtra("from", from);
                intent.putExtra("to", to);
                startActivityForResult(intent, 300);
                //startActivityForResult(new Intent(this, FilterActivity.class), 300);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK){
            if (data != null){
                from = data.getLongExtra("from", 0);
                to = data.getLongExtra("to", 0);
                getData();
            }
        }
    }

    @Override
    public void onCharResult(String textSum, List<UsedApp> list) {
        progressBar.setVisibility(View.GONE);
        textViewSum.setText(textSum);
    }
}

