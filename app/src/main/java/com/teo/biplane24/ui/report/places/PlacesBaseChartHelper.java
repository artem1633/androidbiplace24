package com.teo.biplane24.ui.report.places;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;

import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.teo.biplane24.App;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Tochka;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.BaseCharHelper;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class PlacesBaseChartHelper extends BaseCharHelper implements OnChartValueSelectedListener {




    private int type = 0;
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Map<Integer, ArrayList<TempApp>> map;


    public PlacesBaseChartHelper(Context context, OnCharResultListener listener) {
        this.context = context;
        this.listener = listener;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public void getDataUsers(int app, long from, long to) {

    }



    public boolean checkDistance(Tochka app, Tochka point, int radius) {

        String distance = String.valueOf(calculationByDistance(app, point));

        if (isSuccessRadius(distance, radius)) {

            return true;
        }


        return false;
    }

    private boolean isSuccessRadius(String distance, int radius) {
        try {
            String parseDistance = distance.substring(0, distance.indexOf(".") + 4);
            String[] parse = parseDistance.split("\\.");
            if (parse.length == 2) {
                int km = Integer.parseInt(parse[0]);
                int d = km * 1000;

                int m = Integer.parseInt(parse[1]);
                d += m;


                if (d <= radius) {
                    return true;
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

    private Double calculationByDistance(Tochka StartP, Tochka EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.getLat();
        double lat2 = EndP.getLat();
        double lon1 = StartP.getLng();
        double lon2 = EndP.getLng();
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        return valueResult;
    }
}
