package com.teo.biplane24.ui.report.places;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.FilterActivity;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.BaseReportFragment;
import com.teo.biplane24.ui.report.adapters.ReportPlacesAdapter;
import com.teo.biplane24.ui.report.adapters.ReportUserAdapter;
import com.teo.biplane24.ui.report.adapters.UserPlaceAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportPlacesFragment extends BaseReportFragment implements PlacesBaseChartHelper.OnCharResultListener{

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.item_sum)
    TextView textViewSum;

    @BindView(R.id.spinner_sort)
    Spinner spinnerSort;
    @BindView(R.id.spinner_enable)
    Spinner spinnerEnable;

    @BindView(R.id.item_period)
    TextView textPeriod;
    @BindView(R.id.chart_inflater)
    LinearLayout layoutChar;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;
    @BindView(R.id.item_error)
    TextView textError;
    public ReportPlacesAdapter appAdapter;

    private PlacesBarHelper charHelper;

    private SharedPreferences preferences;
    private int type;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_report, container, false);
        ButterKnife.bind(this, view);

        return view;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);

        textPeriod.setText(filter.getTextPeriod());

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        appAdapter = new ReportPlacesAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(appAdapter);
        appAdapter.setListener(new ReportPlacesAdapter.PlaceClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onPlaceClick(UsedApp item) {

                new UserPlaceTask(){
                    @Override
                    protected void onPostExecute(List<UsedApp> list) {
                        super.onPostExecute(list);
                        UserPlaceAdapter adapter = new UserPlaceAdapter(getContext());
                        View view = LayoutInflater.from(getContext()).inflate(R.layout.activity_user_place, null, false);
                        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(adapter);
                        adapter.setList(list);

                        new AlertDialog.Builder(getContext())
                                .setTitle(item.getName())
                                .setView(view)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                })
                                .show();

                    }
                }.execute(item);




            }
        });

        String[] sortList = new String[]{"По общему времени", "По фИО", "По времени начала", "По времени окончания"};
        String[] enableList = new String[]{"Присутствовал", "Отсутствовал"};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.item_spinner, sortList);
        spinnerSort.setAdapter(arrayAdapter);


        spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ++ib;
                if (ib > 1) {
                    appAdapter.setList(sortList(appAdapter.getList()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> arrayAdapterEnable = new ArrayAdapter<>(getContext(), R.layout.item_spinner, enableList);
        spinnerEnable.setAdapter(arrayAdapterEnable);


        spinnerEnable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ++ab;
                if (ab > 1) {

                    new RoundTask(spinnerEnable.getSelectedItemPosition() == 0).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getData();

    }

    public class UserPlaceTask extends AsyncTask<UsedApp, Void, List<UsedApp>>{
        @Override
        protected List<UsedApp> doInBackground(UsedApp... usedApps) {
            UsedApp item = usedApps[0];
            List<Report> all = App.getInstance().getDatabase().getUserDao().getReports(item.getUser_id());

            List<UsedApp> list = new ArrayList<>();
            UsedApp usedApp = null;

            for (Report report : all){

                if (report.getPlace_id() > 0){
                    if (usedApp != null){
                        if (report.getPlace_id() == usedApp.getUser_id()){
                            long offset = report.getTime_start() - usedApp.getTimeEnd();
                            if (offset > TimeUnit.MINUTES.toMillis(2)){
                                UsedApp app = new UsedApp();
                                app.setUsedTime(usedApp.getUsedTime());
                                app.setTimeStart(usedApp.getTimeStart());
                                app.setTimeEnd(usedApp.getTimeEnd());
                                Place place = App.getInstance().getDatabase().getUserDao().getPlaceId(usedApp.getUser_id());
                                app.setName(place.getName());

                                list.add(app);
                                usedApp = null;
                            }else {
                                usedApp.setTimeEnd(report.getTime_start() + report.getTime_work());
                                usedApp.setUsedTime(usedApp.getUsedTime() + report.getTime_work());
                            }

                        }else {
                            UsedApp app = new UsedApp();
                            app.setUsedTime(usedApp.getUsedTime());
                            app.setTimeStart(usedApp.getTimeStart());
                            app.setTimeEnd(usedApp.getTimeEnd());
                            Place place = App.getInstance().getDatabase().getUserDao().getPlaceId(usedApp.getUser_id());
                            app.setName(place.getName());

                            list.add(app);
                            usedApp = null;
                        }
                    }

                    if (usedApp == null){
                        usedApp = new UsedApp();

                        usedApp.setUser_id(report.getPlace_id());
                        usedApp.setTimeStart(report.getTime_start());
                        usedApp.setTimeEnd(report.getTime_start() + report.getTime_work());
                        usedApp.setUsedTime(report.getTime_work());
                    }
                }else if (usedApp != null){
                    long offset = report.getTime_start() - usedApp.getTimeEnd();

                    if (offset < TimeUnit.MINUTES.toMillis(2)){
                        usedApp.setTimeEnd(report.getTime_start() + report.getTime_work());
                    }else {
                        UsedApp app = new UsedApp();
                        app.setUsedTime(usedApp.getUsedTime());
                        app.setTimeStart(usedApp.getTimeStart());
                        app.setTimeEnd(usedApp.getTimeEnd());
                        Place place = App.getInstance().getDatabase().getUserDao().getPlaceId(usedApp.getUser_id());
                        app.setName(place.getName());

                        list.add(app);
                        usedApp = null;
                    }
                }
            }

            if (usedApp != null){
                UsedApp app = new UsedApp();
                app.setUsedTime(usedApp.getUsedTime());
                app.setTimeStart(usedApp.getTimeStart());
                app.setTimeEnd(usedApp.getTimeEnd());
                Place place = App.getInstance().getDatabase().getUserDao().getPlaceId(usedApp.getUser_id());
                app.setName(place.getName());

                list.add(app);
            }

            return list;
        }


    }

    int ib = 0;
    int ab = 0;

    private List<UsedApp> sortList(List<UsedApp> list){
        UsedApp temp;

        if (spinnerSort.getSelectedItemPosition() == 0){

            for (int i = 0; i < list.size() - 1; i++) {

                for (int j = 1; j < list.size() - i; j++) {
                    if (list.get(j - 1).getUsedTime() < list.get(j).getUsedTime()) {
                        temp = list.get(j - 1);
                        list.set(j - 1, list.get(j));
                        list.set(j, temp);
                    }
                }
            }
        }else if (spinnerSort.getSelectedItemPosition() == 1){

            Collections.sort(list, new Comparator<UsedApp>()
            {
                @Override
                public int compare(UsedApp text1, UsedApp text2)
                {
                    return text1.getName().compareToIgnoreCase(text2.getName());
                }
            });

        }else if (spinnerSort.getSelectedItemPosition() == 2){

            for (int i = 0; i < list.size() - 1; i++) {

                for (int j = 1; j < list.size() - i; j++) {

                        if (list.get(j - 1).getTimeStart() < list.get(j).getTimeStart()) {
                            temp = list.get(j - 1);
                            list.set(j - 1, list.get(j));
                            list.set(j, temp);
                        }


                }
            }
        }else if (spinnerSort.getSelectedItemPosition() == 3){

            for (int i = 0; i < list.size() - 1; i++) {

                for (int j = 1; j < list.size() - i; j++) {

                    if (list.get(j - 1).getTimeEnd() < list.get(j).getTimeEnd()) {
                        temp = list.get(j - 1);
                        list.set(j - 1, list.get(j));
                        list.set(j, temp);
                    }


                }
            }
        }


        return list;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK){
            if (data != null){
                textPeriod.setText(filter.getTextPeriod());
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void getData() {
        if (getActivity() != null){
            filter = App.getInstance().getFilter();
            textPeriod.setText(filter.getTextPeriod());
            appAdapter.clear();
            layoutChar.removeAllViews();
            textError.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            getReports(new OnReportListener() {
                @Override
                public void onSuccess() {
                    getLocalData();
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure() {
                    if (getActivity() != null){
                        progressBar.setVisibility(View.GONE);
                        textError.setVisibility(View.VISIBLE);
                    }

                }
            });

        }

    }

    @Override
    public void getLocalData() {
        if (getActivity() != null){
            progressBar.setVisibility(View.VISIBLE);

            layoutChar.removeAllViews();
            charHelper = new PlacesBarHelper(getContext(), layoutChar, ReportPlacesFragment.this);
            charHelper.setType(preferences.getInt("group_index_s", 0));
            charHelper.setProgress(progressBar);
            charHelper.getDataUsers(0, filter.getFrom(), filter.getTo());
        }
    }

    public class RoundTask extends AsyncTask<Void, Void, List<UsedApp>>{

        public RoundTask(boolean eneble) {
            this.eneble = eneble;
        }

        boolean eneble;
        @Override
        protected List<UsedApp> doInBackground(Void... voids) {
            return charHelper.getUseStaticRound(eneble);
        }

        @Override
        protected void onPostExecute(List<UsedApp> usedApps) {
            super.onPostExecute(usedApps);

            appAdapter.setList(sortList(usedApps));

            if (preferences.getInt("group_index_s", 0) == 1) {
                textViewSum.setText(String.valueOf(usedApps.size()));
            }

        }
    }


    @Override
    public void onCharResult(String textSum, List<UsedApp> list) {
        if (getActivity() != null){
            progressBar.setVisibility(View.GONE);
            textViewSum.setText(textSum);

            new RoundTask(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }
}
