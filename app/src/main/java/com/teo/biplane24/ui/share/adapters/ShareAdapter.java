package com.teo.biplane24.ui.share.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.R;
import com.teo.biplane24.model.Share;
import com.teo.biplane24.model.UsedApp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.ViewHolder>{

    public List<Share> getList() {
        return list;
    }

    public void setList(List<Share> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<Share> list;

    public ShareAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public ShareListener getListener() {
        return listener;
    }

    public void setListener(ShareListener listener) {
        this.listener = listener;
    }

    private ShareListener listener;

    public interface ShareListener{
        void onCopy(Share share);
        void onDelete(Share share);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_share, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Share item = list.get(position);
        holder.textTo.setText(item.getTime_to());
        holder.textFrom.setText(item.getTime_from());
        holder.textComment.setText(item.getComment());

        holder.imageCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCopy(item);
            }
        });

        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDelete(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_from)
        TextView textFrom;
        @BindView(R.id.item_to)
        TextView textTo;
        @BindView(R.id.item_comment)
        TextView textComment;
        @BindView(R.id.item_copy)
        ImageView imageCopy;
        @BindView(R.id.item_delete)
        ImageView imageDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
