package com.teo.biplane24.ui.place;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.teo.biplane24.App;
import com.teo.biplane24.AppLockService;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Place;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPlaceActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_inflate_point)
    LinearLayout layout;
    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.edit_radius)
    EditText editRadius;
    @BindView(R.id.item_add_point)
    ImageView imageAdd;

    @BindView(R.id.layout_content)
    LinearLayout layoutContent;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private int count = 0;

    private int locationCount;


    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        checkLocation();

    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Location location = AppLockService.getLocation();
            if (location != null){
                layoutContent.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

                addPoint();
            }else {
                locationCount += 1;
                if (locationCount >= 4){
                    Toast.makeText(AddPlaceActivity.this, "Координаты не удалось получить!", Toast.LENGTH_SHORT).show();
                    finish();
                }else {
                    handler.postDelayed(runnable, 1000);
                }

            }
        }
    };


    private void  checkLocation(){
        handler.post(runnable);
    }

    @OnClick(R.id.item_add_point)
    public void addPoint(){
        ++ count;
        if (count >= 4){
            imageAdd.setVisibility(View.GONE);
        }

        View root = LayoutInflater.from(this).inflate(R.layout.item_add_point, layout, false);
        ImageView button = root.findViewById(R.id.item_delete);
        if (count == 1){
            button.setVisibility(View.GONE);
        }else {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    layout.removeView(root);
                    --count;
                    imageAdd.setVisibility(View.VISIBLE);
                }
            });
        }

        TextView textView = root.findViewById(R.id.item_point);

        Location location = AppLockService.getLocation();
        if (location != null){
            String point = location.getLatitude() + "," + location.getLongitude();
            textView.setText(point);
        }


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        layout.addView(root);

    }

    @OnClick(R.id.button_save)
    public void save(){

        String name = editName.getText().toString();
        String radius = editRadius.getText().toString();

        Map<String, String> map = new HashMap<>();
        map.put("token", App.getInstance().getUser().getToken());
        map.put("name", name);

        for (int i = 0; i < layout.getChildCount(); i++){
            int position = i + 1;
            View view = layout.getChildAt(i);
            TextView textView = view.findViewById(R.id.item_point);
            map.put("position" + position, textView.getText().toString());
        }


        map.put("radius", radius);
        Call<Place> call = App.getRetrofitApi().createPlace(map);
        call.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {
                if (response != null && response.body() != null){
                    App.getInstance().getDatabase().getUserDao().insertPlace(response.body());
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Place> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        handler.removeCallbacks(runnable);

        super.onDestroy();
    }
}
