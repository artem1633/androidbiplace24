package com.teo.biplane24.ui.report.apps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AppsStackedHelper extends AppsBaseChartHelper {

    private BarChart chart;

    public AppsStackedHelper(Context context, LinearLayout root, OnCharResultListener listener) {
        super(context, listener);

        chart = new BarChart(context);
        chart.setNoDataText(context.getResources().getString(R.string.char_empty));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        root.addView(chart, params);

        chart.setOnChartValueSelectedListener(this);
        chart.getDescription().setEnabled(false);

        chart.setDrawBorders(false);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawBarShadow(false);

        chart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it

        /*Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);*/

        //MyMarkerView mv = new MyMarkerView(context, R.layout.custom_marker_view);
        //mv.setChartView(chart); // For bounds control
        //chart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = chart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setLabelCount(labelCount);
        xAxis.setCenterAxisLabels(false);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return getFormatX(value);
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return getFormatY(value);
            }
        });

        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void getDataApps(String app, long from, long to) {

        this.from = from;
        this.to = to;


        new CharTask(progress){
            @Override
            protected Boolean doInBackground(Void... voids) {
                super.doInBackground(voids);
                return isUseStaticApps(app);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {


                if (aBoolean){
                    List<IBarDataSet> barDataSets = new ArrayList<>();

                    for (TempDataSet tempDataSet : tempDataSets){
                        List<BarEntry> barEntries = new ArrayList<>();
                        int[] colors = new int[tempDataSet.getAppNames().size()];

                        for (int i = 0; i < tempDataSet.getAppNames().size(); i++) {
                            String appName = tempDataSet.getAppNames().get(i);

                            colors[i] = App.getInstance().getColorApp(appName);

                        }

                        barEntries.add(new BarEntry(tempDataSet.getPeriod(), tempDataSet.getYs(), "test"));

                        BarDataSet barDataSet = new BarDataSet(barEntries, tempDataSet.getAppName());
                        barDataSet.setDrawIcons(false);
                        barDataSet.setColors(colors);
                        barDataSets.add(barDataSet);
                    }

                    BarData data = new BarData(barDataSets);

                    data.setValueFormatter(new ValueFormatter() {


                        @Override
                        public String getBarLabel(BarEntry barEntry) {
                            return getFormatY(barEntry.getY());
                        }
                        @Override
                        public String getBarStackedLabel(float value, BarEntry entry) {

                            float[] vals = entry.getYVals();

                            if (vals != null && vals.length > 1) {

                                if (vals[vals.length - 1] == value) {

                                    float sum = 0;
                                    for (float v : vals){
                                        sum += v;
                                    }

                                    return getFormatY(sum);
                                } else {
                                    return ""; // return empty
                                }
                            }

                            return getFormatY(entry.getY());
                        }
                    });


                    chart.setData(data);

                    chart.setFitBars(true);
                    chart.invalidate();
                    listener.onCharResult(TimeUtil.convertMillis(d), usedAppList);
                }else {
                    listener.onCharResult("00:00:00", usedAppList);
                }

                super.onPostExecute(aBoolean);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }



    @Override
    public boolean isUseStaticApps(String appSelect) {
        map = new HashMap<>();
        usedAppList = new ArrayList<>();
        List<Report> all = App.getInstance().getDatabase().getUserDao().getReports();
        if (all.size() > 0) {


            int i = 0;


            periods = new ArrayList<>();
            tempDataSets = new ArrayList<>();

            while (from < to) {
                long p = from + period;
                //Log.e("tr", "FROM " + getFormatDate(from));
                periods.add(from);

                List<Report> list;
                if (appSelect == null){
                    list = App.getInstance().getDatabase().getUserDao().getReports(from, p);
                }else {
                    list = App.getInstance().getDatabase().getUserDao().getReports(from, p, appSelect);
                }

                List<String> appNames = new ArrayList<>();
                float[] vals = new float[1];
                int v = 0;

                if (list != null && list.size() > 0) {

                    Map<String, List<Report>> mapTime = new HashMap<>();

                    for (Report appTime : list) {

                        List<Report> timeList = mapTime.get(appTime.getApp_id());
                        if (timeList == null) {
                            timeList = new ArrayList<>();
                        }

                       // Log.e("tr", "     " + appTime.getUser_id() + " " + getFormatDate(appTime.getTime_start()) + " " +  convertSecondsToHMmSs(appTime.getTime_work()));

                        timeList.add(appTime);

                        mapTime.put(appTime.getApp_id(), timeList);
                    }

                    Iterator<Map.Entry<String, List<Report>>> it = mapTime.entrySet().iterator();

                    long durationTimes;


                    //if (appSelect == null) {
                        vals = new float[mapTime.size()];

                        while (it.hasNext()) {

                            durationTimes = 0;

                            Map.Entry<String, List<Report>> pair = it.next();
                            String app = pair.getKey();

                            appNames.add(app);

                            List<Report> timeList = pair.getValue();

                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }

                            vals[v] = durationTimes;
                            ++v;

                            if (durationTimes > 0) {

                                d += durationTimes;

                                ArrayList<TempApp> mapList = map.get(app);
                                if (mapList == null) {
                                    mapList = new ArrayList<>();
                                }

                                mapList.add(new TempApp(i, durationTimes, durationTimes));
                                map.put(app, mapList);

                            }
                        }
                }

                if (appNames.size() == 0){
                    appNames.add("empty");
                }


                TempDataSet set = new TempDataSet();
                set.setAppNames(appNames);
                set.setYs(vals);
                set.setPeriod(i);
                tempDataSets.add(set);

                i++;
                from += period;
            }

            //Log.e("tr", "periods " + periods.size());

            Iterator<Map.Entry<String, ArrayList<TempApp>>> it = map.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, ArrayList<TempApp>> pair = it.next();

                ArrayList<TempApp> entries = pair.getValue();
                String key = pair.getKey();

                long duration = 0;

                for (TempApp entry : entries) {
                    duration += entry.getDuration();
                }

                UsedApp usedApp = new UsedApp();
                usedApp.setApp(key);
                usedApp.setColor(App.getInstance().getColorApp(key));
                usedApp.setUsedTime(duration);

                usedApp.setName(getAppNameFromPkgName(context, key));
                usedAppList.add(usedApp);
            }

            usedAppList = sortList(usedAppList);


            return true;
        }

        return false;
    }

    /*
    http://biplane24.teo-crm.com/api/report/index?token=hxZXfOT3oz2OYj7XA2F7wXPX7kdZE9LT&datetime_start=2019-11-27%2008%3A58&datetime_end=2019-11-28%2008%3A58 (180ms)

    FROM 2019-11-27 21:47:05
         3 2019-11-27 21:48:14 00:00:03
              3 2019-11-27 21:48:14 00:03:20
              3 2019-11-27 21:51:35 00:00:01
              3 2019-11-27 21:51:35 00:02:28
     */


    public String getAppNameFromPkgName(Context context, String Packagename) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo info = packageManager.getApplicationInfo(Packagename, PackageManager.GET_META_DATA);
            String appName = (String) packageManager.getApplicationLabel(info);
            return appName;
        } catch (PackageManager.NameNotFoundException e) {
            //e.printStackTrace();
            return Packagename;
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
