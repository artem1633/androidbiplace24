package com.teo.biplane24.ui.report.apps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.FilterActivity;
import com.teo.biplane24.R;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.BaseReportFragment;
import com.teo.biplane24.ui.report.adapters.ReportAppAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportAppFragment extends BaseReportFragment implements AppsBaseChartHelper.OnCharResultListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.item_sum)
    TextView textViewSum;

    @BindView(R.id.item_period)
    TextView textPeriod;
    @BindView(R.id.chart_inflater)
    LinearLayout layoutChar;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;
    @BindView(R.id.item_error)
    TextView textError;
    public ReportAppAdapter appAdapter;

    private AppsBaseChartHelper charHelper;

    private SharedPreferences preferences;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_report, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
        textPeriod.setText(filter.getTextPeriod());

        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        appAdapter = new ReportAppAdapter(getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(appAdapter);

        appAdapter.setListener(new ReportAppAdapter.AppClickListener() {
            @Override
            public void onAppClick(UsedApp item) {
                Intent intent = new Intent(getContext(), AppReportActivity.class);
                intent.putExtra("app", item);
                intent.putExtra("from", filter.getFrom());
                intent.putExtra("to", filter.getTo());
                startActivity(intent);

            }
        });

        getData();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK){
            if (data != null){

                textPeriod.setText(filter.getTextPeriod());

            }

        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void getData() {

        if (getActivity() != null){
            filter = App.getInstance().getFilter();
            textPeriod.setText(filter.getTextPeriod());
            appAdapter.clear();
            layoutChar.removeAllViews();
            textError.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            getReports(new OnReportListener() {
                @Override
                public void onSuccess() {
                    getLocalData();
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure() {
                    if (getActivity() != null){
                        progressBar.setVisibility(View.GONE);
                        textError.setVisibility(View.VISIBLE);
                    }

                }
            });
        }


    }



    @Override
    public void getLocalData() {

        if (getActivity() != null){
            progressBar.setVisibility(View.VISIBLE);
            layoutChar.removeAllViews();
            int type = preferences.getInt("type", 1);
            switch (type){
                case 1:
                    charHelper = new AppsStackedHelper(getContext(), layoutChar,ReportAppFragment.this);
                    break;
                case 2:
                    charHelper = new AppsLineHelper(getContext(), layoutChar, ReportAppFragment.this);
                    break;
                case 3:
                    charHelper = new AppsPieHelper(getContext(), layoutChar, ReportAppFragment.this);
                    break;
            }

            charHelper.setProgress(progressBar);
            charHelper.getDataApps(null, filter.getFrom(), filter.getTo());
        }
    }


    @Override
    public void onCharResult(String textSum, List<UsedApp> list) {
        if (getActivity() != null){
            textViewSum.setText(textSum);
            if (list == null) {
                list = new ArrayList<>();
            }

            appAdapter.setList(list);
            progressBar.setVisibility(View.GONE);
        }
    }
}
