package com.teo.biplane24.ui.round;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Share;
import com.teo.biplane24.ui.round.adapters.RoundAdapter;
import com.teo.biplane24.ui.share.AddShareActivity;
import com.teo.biplane24.ui.share.adapters.ShareAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoundFragment extends Fragment {



    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.button_add)
    Button buttonAdd;

    private RoundAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_environment, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RoundAdapter(getContext());
        recyclerView.setAdapter(adapter);

        ClipboardManager clipboardManager = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                Toast.makeText(getContext(), "Скопировано!", Toast.LENGTH_SHORT).show();
            }
        });
        adapter.setListener(new RoundAdapter.RoundListener() {

            @Override
            public void onDelete(Round share) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.app_name)
                        .setMessage("Удалить?")
                        .setPositiveButton("ок", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Call<String> call = App.getRetrofitApi().deleteRound(App.getInstance().getUser().getToken(), share.getId());
                                call.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        if (response != null && response.body() != null){
                                            App.getInstance().getDatabase().getUserDao().deleteRound(share);
                                            adapter.getList().remove(share);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {

                                    }
                                });
                            }
                        })
                        .setNegativeButton("Отмена", null)
                        .show();
            }
        });
    }

    @Override
    public void onResume() {

        super.onResume();
        List<Round> list1 = App.getInstance().getDatabase().getUserDao().getRounds();
        List<Round> list = new ArrayList<>();
        for (Round round : list1){
            Log.e("tr", "round " + round.getId() + " " + round.getHref_user_id());
            if (round.getHref_user_id() != App.getInstance().getUser().getId()){
                list.add(round);
            }
        }
        adapter.setList(list);
    }

    @OnClick(R.id.button_add)
    public void add(){

        startActivity(new Intent(getContext(), AddRoundActivity.class));
    }
}