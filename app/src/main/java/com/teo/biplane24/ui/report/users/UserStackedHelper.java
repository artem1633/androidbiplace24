package com.teo.biplane24.ui.report.users;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class UserStackedHelper extends UsersBaseChartHelper {


    private BarChart chart;

    public UserStackedHelper(Context context, LinearLayout root, OnCharResultListener listener) {
        super(context, listener);

        chart = new BarChart(context);
        chart.setNoDataText(context.getResources().getString(R.string.char_empty));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        root.addView(chart, params);

        chart.setOnChartValueSelectedListener(this);
        chart.getDescription().setEnabled(false);

        chart.setDrawBorders(false);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawBarShadow(false);

        chart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it

        /*Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);*/

        //MyMarkerView mv = new MyMarkerView(context, R.layout.custom_marker_view);
        //mv.setChartView(chart); // For bounds control
        //chart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = chart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setLabelCount(labelCount);
        xAxis.setCenterAxisLabels(false);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return getFormatX(value);
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return getFormatY(value);
            }
        });

        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void getDataUsers(int app, long from, long to) {

        this.from = from;
        this.to = to;

        new CharTask(progress){
            @Override
            protected Boolean doInBackground(Void... voids) {
                super.doInBackground(voids);
                return isUseStaticUsers(app);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {

                if (aBoolean){
                    List<IBarDataSet> barDataSets = new ArrayList<>();

                    for (TempDataSet tempDataSet : tempDataSets){
                        List<BarEntry> barEntries = new ArrayList<>();
                        int[] colors = new int[tempDataSet.getAppNames().size()];

                        //Log.e("tr", "names " + tempDataSet.getAppNames().size());

                        for (int i = 0; i < tempDataSet.getAppNames().size(); i++) {
                            String appName = tempDataSet.getAppNames().get(i);
                            colors[i] = App.getInstance().getColorApp(appName);
                            //Log.e("tr", "app " + appName + " color " + colors[i]);
                        }

                        barEntries.add(new BarEntry(tempDataSet.getPeriod(), tempDataSet.getYs(), "test"));

                        BarDataSet barDataSet = new BarDataSet(barEntries, tempDataSet.getAppName());
                        barDataSet.setDrawIcons(false);
                        barDataSet.setColors(colors);
                        barDataSets.add(barDataSet);
                    }

                    BarData data = new BarData(barDataSets);

                    data.setValueFormatter(new ValueFormatter() {

                        @Override
                        public String getBarLabel(BarEntry barEntry) {
                            return getFormatY(barEntry.getY());

                        }

                        @Override
                        public String getBarStackedLabel(float value, BarEntry entry) {

                            float[] vals = entry.getYVals();

                            if (vals != null) {

                                if (vals[vals.length - 1] == value) {

                                    float sum = 0;
                                    for (float v : vals){
                                        sum += v;
                                    }

                                    return getFormatY(sum);
                                } else {
                                    return ""; // return empty
                                }
                            }

                            return getFormatY(entry.getY());
                        }
                    });


                    chart.setData(data);

                    chart.setFitBars(true);
                    chart.invalidate();

                    listener.onCharResult(TimeUtil.convertMillis(d), usedAppList);
                }else {
                    listener.onCharResult("00:00:00", usedAppList);
                }

                super.onPostExecute(aBoolean);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public boolean isUseStaticUsers(int appSelect) {
        map = new HashMap<>();
        List<Report> all = App.getInstance().getDatabase().getUserDao().getReports();

        usedAppList = new ArrayList<>();
        tempDataSets = new ArrayList<>();

        if (all.size() > 0) {

            int i = 0;

            periods = new ArrayList<>();
            while (from < to) {
                long p = from + period;

                periods.add(from);

                List<Report> list = App.getInstance().getDatabase().getUserDao().getReports(from, p);

                List<String> appNames = new ArrayList<>();

                float[] vals = new float[1];
                int v = 0;

                if (list != null && list.size() > 0) {

                    Map<Integer, List<Report>> mapTime = new HashMap<>();

                    for (Report appTime : list) {

                        Round round = App.getInstance().getDatabase().getUserDao().getRound(appTime.getUser_id());

                        if (round != null){
                            List<Report> timeList = mapTime.get(round.getId());
                            if (timeList == null) {
                                timeList = new ArrayList<>();
                            }

                            timeList.add(appTime);

                            mapTime.put(round.getId(), timeList);
                        }
                    }

                    Iterator<Map.Entry<Integer, List<Report>>> it = mapTime.entrySet().iterator();

                    if (appSelect == 0) {
                        vals = new float[mapTime.size()];

                        while (it.hasNext()) {

                            Map.Entry<Integer, List<Report>> pair = it.next();
                            int app = pair.getKey();

                            //Log.e("tr", "app " + app);
                            List<Report> timeList = pair.getValue();

                            long durationTimes = 0;
                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }

                            vals[v] = durationTimes;
                            ++v;

                            appNames.add(app + "user");

                            if (durationTimes > 0) {


                                d += durationTimes;

                                ArrayList<TempApp> mapList = map.get(app);
                                if (mapList == null) {
                                    mapList = new ArrayList<>();
                                }

                                mapList.add(new TempApp(i, durationTimes, durationTimes));

                                map.put(app, mapList);


                            }
                        }
                    }else {
                        long durationTimes = 0;
                        vals = new float[1];
                        List<Report> timeList = mapTime.get(appSelect);

                        appNames.add(appSelect + "user");

                        if (timeList != null){
                            for (Report appTime : timeList) {
                                durationTimes += appTime.getTime_work();
                            }
                        }

                        if (durationTimes > 0) {

                            vals[0] = durationTimes;

                            d += durationTimes;

                            ArrayList<TempApp> mapList = map.get(appSelect);
                            if (mapList == null) {
                                mapList = new ArrayList<>();
                            }

                            mapList.add(new TempApp(i, durationTimes, durationTimes));

                            map.put(appSelect, mapList);
                        }
                    }

                }

                if (appNames.size() == 0){
                    appNames.add("empty");
                }

                TempDataSet set = new TempDataSet();
                //Log.e("tr", "names root " + appNames.size());
                set.setAppNames(appNames);
                set.setYs(vals);
                set.setPeriod(i);

                tempDataSets.add(set);

                i++;
                from += period;
            }


            Iterator<Map.Entry<Integer, ArrayList<TempApp>>> it = map.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<Integer, ArrayList<TempApp>> pair = it.next();

                ArrayList<TempApp> entries = pair.getValue();
                int key = pair.getKey();

                long duration = 0;

                for (TempApp entry : entries) {
                    duration += entry.getDuration();
                }

                Round round = App.getInstance().getDatabase().getUserDao().getRoundId(key);

                if (round != null){
                    UsedApp usedApp = new UsedApp();
                    usedApp.setUser_id(key);
                    usedApp.setColor(App.getInstance().getColorApp(key + "user"));
                    usedApp.setUsedTime(duration);
                    usedApp.setName(round.getName());
                    usedAppList.add(usedApp);
                }
            }

            usedAppList = sortList(usedAppList);


            return true;

        }



        return false;
    }



    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

}
