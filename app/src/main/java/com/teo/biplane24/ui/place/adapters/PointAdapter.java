package com.teo.biplane24.ui.place.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.teo.biplane24.R;
import com.teo.biplane24.model.Place;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PointAdapter  extends RecyclerView.Adapter<PointAdapter.ViewHolder>{

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
        Log.e("tr", "count " + list.size());

        notifyDataSetChanged();
    }

    private List<String> list;

    public PointAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    private Context context;

    public PlaceListener getListener() {
        return listener;
    }

    public void setListener(PlaceListener listener) {
        this.listener = listener;
    }

    private PlaceListener listener;

    public interface PlaceListener {
        void onClick(String place);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_point, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final String item = list.get(position);
        holder.textName.setText(item);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(item);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_name)
        TextView textName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

}