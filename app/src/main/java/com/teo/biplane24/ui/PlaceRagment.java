package com.teo.biplane24.ui;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceRagment extends Fragment {

    @BindView(R.id.item_spinner)
    Spinner spinner;
    @BindView(R.id.item_add)
    ImageView buttonAdd;
    @BindView(R.id.layout_select)
    LinearLayout layoutSelect;
    @BindView(R.id.item_progress)
    ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragent_group, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private List<Place> list;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        buttonAdd.setVisibility(View.GONE);
        list = new ArrayList<>();

        Place status = new Place();
        status.setName("Все места");
        status.setId(0);
        list.add(status);

        list.addAll(App.getInstance().getDatabase().getUserDao().getPlaces());
        layoutSelect.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        ArrayAdapter<Place> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(arrayAdapter);
        //spinner.setSelection(preferences.getInt("place_selected", 0));
        int select = App.getInstance().getFilter().getPlace();
        for (int i = 0; i < list.size(); i++){
            Place item = list.get(i);
            if (item.getId() == select){
                select = i;
            }
        }
        if (select < list.size()) {
            spinner.setSelection(select);
        }

    }

    public Place getSelect() {

        return (Place) spinner.getSelectedItem();

    }


}
