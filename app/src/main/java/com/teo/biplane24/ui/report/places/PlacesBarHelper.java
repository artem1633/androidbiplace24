package com.teo.biplane24.ui.report.places;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.teo.biplane24.App;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Report;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.model.Tochka;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PlacesBarHelper extends PlacesBaseChartHelper {


    private BarChart chart;


    public PlacesBarHelper(Context context, LinearLayout root, OnCharResultListener listener) {
        super(context, listener);


        chart = new BarChart(context);
        chart.setNoDataText(context.getResources().getString(R.string.char_empty));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        root.addView(chart, params);


        chart.setOnChartValueSelectedListener(this);
        chart.getDescription().setEnabled(false);

        chart.setDrawBorders(false);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawBarShadow(false);

        chart.setDrawGridBackground(false);

        XAxis xAxis = chart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setLabelCount(labelCount);

        xAxis.setCenterAxisLabels(false);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return getFormatX(value);
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {

                //return getFormatY(value);

                if (getType() == 0)
                    return getFormatY(value);

                if (value > 0)
                    return String.valueOf((int) value);

                return "";
            }
        });

        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);


    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void getDataUsers(int app, long from, long to) {

        this.from = from;
        this.to = to;




        new CharTask(progress) {
            @Override
            protected Boolean doInBackground(Void... voids) {
                super.doInBackground(voids);
                return getType() == 0 ? isUseStaticPlace() : isUseStaticUsers();
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {

                if (aBoolean) {
                    List<IBarDataSet> barDataSets = new ArrayList<>();

                    for (TempDataSet tempDataSet : tempDataSets){
                        List<BarEntry> barEntries = new ArrayList<>();
                        int[] colors = new int[tempDataSet.getAppNames().size()];

                        for (int i = 0; i < tempDataSet.getAppNames().size(); i++) {
                            String appName = tempDataSet.getAppNames().get(i);

                            colors[i] = App.getInstance().getColorApp(appName);

                        }

                        barEntries.add(new BarEntry(tempDataSet.getPeriod(), tempDataSet.getYs(), "test"));

                        BarDataSet barDataSet = new BarDataSet(barEntries, tempDataSet.getAppName());
                        barDataSet.setDrawIcons(false);
                        barDataSet.setColors(colors);
                        barDataSets.add(barDataSet);
                    }

                    BarData data = new BarData(barDataSets);



                    /*List<IBarDataSet> barDataSets = new ArrayList<>();

                    for (TempDataSet tempDataSet : tempDataSets) {
                        List<BarEntry> barEntries = new ArrayList<>();
                        int[] colors = new int[tempDataSet.getList().size()];

                        for (int i = 0; i < tempDataSet.getList().size(); i++) {
                            TempApp tempApp = tempDataSet.getList().get(i);
                            colors[i] = App.getInstance().getColorApp(tempApp.getUser() + "user");
                            barEntries.add(new BarEntry(tempApp.getX(), tempApp.getYs(), "test"));
                        }

                        BarDataSet barDataSet = new BarDataSet(barEntries, tempDataSet.getAppName());
                        barDataSet.setDrawIcons(false);
                        barDataSet.setColors(colors);
                        barDataSets.add(barDataSet);
                    }*/

                    //BarData data = new BarData(barDataSets);

                    data.setValueFormatter(new ValueFormatter() {

                        @Override
                        public String getBarLabel(BarEntry entry) {


                            if (getType() == 0){
                                float[] vals = entry.getYVals();

                                if (vals != null) {

                                    float sum = 0;
                                    for (float v : vals) {
                                        sum += v;
                                    }

                                    if (getType() == 0)
                                        return getFormatY(sum);


                                    return String.valueOf((int) sum);

                                }

                                return getFormatY(entry.getY());
                            }

                            if (entry.getY() > 0)
                                return String.valueOf((int) entry.getY());

                            return "";

                        }

                        @Override
                        public String getBarStackedLabel(float value, BarEntry entry) {


                            float[] vals = entry.getYVals();

                            if (vals != null) {

                                if (value == 0.01f) {

                                    float sum = 0;
                                    for (float v : vals) {
                                        sum += v;
                                    }

                                    if (getType() == 0) {
                                        return getFormatY(sum);
                                    }else {
                                        return String.valueOf((int)sum);
                                    }


                                } else {
                                    return ""; // return empty
                                }
                            }

                            return getFormatY(entry.getY());
                        }


                    });


                    chart.setData(data);

                    chart.setFitBars(true);
                    chart.invalidate();
                    listener.onCharResult(getType() == 0 ? TimeUtil.convertMillis(d) : String.valueOf(d), null);
                } else {
                    listener.onCharResult(getType() == 0 ? "00:00" : "0", null);
                }

                super.onPostExecute(aBoolean);

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private boolean log = false;
    public boolean isUseStaticPlace() {
        map = new HashMap<>();

        tempDataSets = new ArrayList<>();

        int i = 0;

        periods = new ArrayList<>();

        while (from < to) {

            long p = from + period;
            periods.add(from);
            List<Report> list = App.getInstance().getDatabase().getUserDao().getReports(from, p);

            //ArrayList<TempApp> values = new ArrayList<>();

            List<String> appNames = new ArrayList<>();

            float[] vals = new float[1];

            if (list != null && list.size() > 0) {

                Map<String, List<Report>>  mapPlace = new HashMap<>();

                for (Report appTime : list) {

                    Place place = App.getInstance().getDatabase().getUserDao().getPlaceId(appTime.getPlace_id());

                    if (place != null){


                        List<Report> timeList = mapPlace.get("place" + appTime.getPlace_id());
                        if (timeList == null) {
                            timeList = new ArrayList<>();
                        }


                        timeList.add(appTime);

                        mapPlace.put("place" + appTime.getPlace_id(), timeList);

                    }else {
                        Log.e("tr", "no place " + appTime.getId() + " " + appTime.getTime_start() + " " + appTime.getDatetime());
                    }

                }

                //Log.e("tr", "map " + mapPlace.size());


                Iterator<Map.Entry<String, List<Report>>> it = mapPlace.entrySet().iterator();

                vals = new float[mapPlace.size()];
                int v = 0;

                while (it.hasNext()){
                    long durationTimes = 0;


                    Map.Entry<String, List<Report>> pair = it.next();
                    String app = pair.getKey();

                    appNames.add(app);

                    List<Report> timeList = pair.getValue();

                    for (Report appTime : timeList) {
                        durationTimes += appTime.getTime_work();
                    }

                    vals[v] = durationTimes;
                    ++v;

                    d += durationTimes;


                    //Log.e("tr", "d " + durationTimes);

                }

            }

            if (appNames.size() == 0){
                appNames.add("empty");
            }

            TempDataSet set = new TempDataSet();
            set.setAppNames(appNames);
            set.setYs(vals);
            set.setPeriod(i);
            tempDataSets.add(set);

            //TempDataSet set = new TempDataSet(values, "test");

            //tempDataSets.add(set);

            from += period;
            ++i;
        }

        if (tempDataSets.size() > 0) {

            return true;

        }


        return false;
    }



    public boolean isUseStaticUsers() {
        map = new HashMap<>();

        tempDataSets = new ArrayList<>();

        int i = 0;

        periods = new ArrayList<>();

        List<String> userList = new ArrayList<>();

        while (from < to) {

            long p = from + period;
            periods.add(from);
            List<Report> list = App.getInstance().getDatabase().getUserDao().getReports(from, p);


            //ArrayList<TempApp> values = new ArrayList<>();

            List<String> appNames = new ArrayList<>();

            float[] vals = new float[1];

            if (list != null && list.size() > 0) {

                Map<String, List<Report>>  mapPlace = new HashMap<>();

                for (Report appTime : list) {

                    Round place = App.getInstance().getDatabase().getUserDao().getRound(appTime.getUser_id());
                    if (place != null){

                        List<Report> timeList = mapPlace.get("user" + appTime.getUser_id());
                        if (timeList == null) {
                            timeList = new ArrayList<>();
                        }

                        timeList.add(appTime);

                        mapPlace.put("user" + appTime.getUser_id(), timeList);


                        if (!userList.contains("user"+ appTime.getUser_id())){
                            userList.add("user" + appTime.getUser_id());
                        }
                    }
                }


                Iterator<Map.Entry<String, List<Report>>> it = mapPlace.entrySet().iterator();

                vals = new float[mapPlace.size() + 1];
                int v = 0;

                while (it.hasNext()){

                    Map.Entry<String, List<Report>> pair = it.next();
                    String app = pair.getKey();

                    appNames.add(app);

                    vals[v] = 1;
                    ++v;

                }

                appNames.add("transparent");
                vals[v] = 0.01f;

            }

            if (appNames.size() == 0){
                appNames.add("empty");
            }

            TempDataSet set = new TempDataSet();
            set.setAppNames(appNames);
            set.setYs(vals);
            set.setPeriod(i);
            tempDataSets.add(set);


            from += period;
            ++i;
        }

        d = userList.size();

        if (tempDataSets.size() > 0) {

            return true;

        }


        return false;
    }

    public List<UsedApp> getUseStaticRound(boolean enable) {
        map = new HashMap<>();

        List<UsedApp> usedAppList = new ArrayList<>();
        List<Round> rounds = App.getInstance().getDatabase().getUserDao().getRounds();

        for (Round round : rounds) {

            long duration = 0;
            UsedApp usedApp = new UsedApp();


            List<Report> all = App.getInstance().getDatabase().getUserDao().getReports(round.getHref_user_id());
            if (all != null && all.size() > 0) {
                for (Report report : all) {
                    if (report.getPlace_id() > 0){
                        duration += report.getTime_work();
                    }
                }

                usedApp.setTimeStart(all.get(0).getTime_start());
                usedApp.setTimeEnd(all.get(all.size() - 1).getTime_start() + all.get(all.size() - 1).getTime_work());
            }

            usedApp.setColor(App.getInstance().getColorApp("user" + round.getHref_user_id()));
            Group group = App.getInstance().getDatabase().getUserDao().getGroup(round.getGroup_id());
            Status status = App.getInstance().getDatabase().getUserDao().getStatus(round.getStatus_id());

            usedApp.setTempApps(all);

            if (group != null){
                usedApp.setGroup(group.getName());
            }

            if (status != null){
                usedApp.setStatus(status.getName());
            }

            usedApp.setUser_id(round.getHref_user_id());
            usedApp.setUsedTime(duration);
            usedApp.setName(round.getName());

            if (duration > 0 && enable) {
                usedAppList.add(usedApp);
            }else if (!enable && duration == 0){
                usedAppList.add(usedApp);
            }
        }

        return usedAppList;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
