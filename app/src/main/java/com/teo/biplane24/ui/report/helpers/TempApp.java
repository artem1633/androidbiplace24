package com.teo.biplane24.ui.report.helpers;

public class TempApp {

    public TempApp(float x, float y, float duration) {
        this.x = x;
        this.y = y;
        this.duration = duration;
    }

    public TempApp(float x, float[] y, String app) {
        this.x = x;
        this.ys = y;
        this.app = app;
    }

    public TempApp(float x, float[] y, int app) {
        this.x = x;
        this.ys = y;
        this.user = app;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    private String app;

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    private int user;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    private float x;

    public float[] getYs() {
        return ys;
    }

    public void setYs(float[] ys) {
        this.ys = ys;
    }

    private float[] ys;
    private float y;
    private float duration;
}
