package com.teo.biplane24.ui.report;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.View;

import com.teo.biplane24.TimeUtil;
import com.teo.biplane24.model.UsedApp;
import com.teo.biplane24.ui.report.helpers.TempApp;
import com.teo.biplane24.ui.report.helpers.TempDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BaseCharHelper {

    public long d = 0;

    public long from;
    public long to;
    public int periodId;
    public long period;
    public int labelCount = 7;
    public int padintPie = 10;

    public OnCharResultListener listener;

    public int maxLegendCount = 10;
    public List<Long> periods;
    public List<UsedApp> usedAppList;

    public List<TempDataSet> tempDataSets;

    public Context context;
    public SharedPreferences preferences;

    public View getProgress() {
        return progress;
    }

    public void setProgress(View progress) {
        this.progress = progress;
    }

    public View progress;

    public class CharTask extends AsyncTask<Void, Void, Boolean>{

        public CharTask(View view) {
            this.view = view;
        }



        private View view;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            view.setVisibility(View.GONE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            period = preferences.getLong("period", TimeUnit.MINUTES.toMillis(10));
            periodId = preferences.getInt("group_period_s", 0);
            return null;
        }
    }

    public interface OnCharResultListener {
        void onCharResult(String textSum, List<UsedApp> list);
    }

    public String getFormatDate(long date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fo =  format.format(new Date(date));

        return fo;
    }

    public String getFormatX(float value) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        switch (periodId) {
            case 0:
            case 1:
                format = new SimpleDateFormat("HH:mm");
                break;
            case 2:
            case 3:
                format = new SimpleDateFormat("dd-MM");
                break;
            case 4:
                format = new SimpleDateFormat("MMMM-yyyy");
                break;
        }

        int intPeriod = (int) value;
        if (intPeriod >= 0 && intPeriod < periods.size()) {
            long period = periods.get(intPeriod);
            return format.format(new Date(period));
        }

        return "";
    }

    public String getFormatY(float value) {
        long v = (int) value;
        if (v > 0)
            return TimeUtil.convertMillis(v);

        return "";
    }

    public List<UsedApp> sortList(List<UsedApp> list) {
        UsedApp temp;

        for (int i = 0; i < list.size() - 1; i++) {

            for (int j = 1; j < list.size() - i; j++) {
                if (list.get(j - 1).getUsedTime() < list.get(j).getUsedTime()) {
                    temp = list.get(j - 1);
                    list.set(j - 1, list.get(j));
                    list.set(j, temp);
                }
            }
        }

        return list;
    }


}
