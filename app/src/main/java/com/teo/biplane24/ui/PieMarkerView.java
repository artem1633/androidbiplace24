package com.teo.biplane24.ui;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.teo.biplane24.R;
import com.teo.biplane24.TimeUtil;

public class PieMarkerView extends MarkerView {
    private final TextView tvContent;

    public PieMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = findViewById(R.id.tvContent);
    }



    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        PieEntry pieEntry = (PieEntry)e;
        tvContent.setText(pieEntry.getLabel());// + " - " + TimeUtil.convertMillis((long) pieEntry.getValue()));
        super.refreshContent(e, highlight);
    }
}
