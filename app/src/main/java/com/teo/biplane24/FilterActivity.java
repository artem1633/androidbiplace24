package com.teo.biplane24;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.teo.biplane24.model.AppTime;
import com.teo.biplane24.model.Group;
import com.teo.biplane24.model.Place;
import com.teo.biplane24.model.Round;
import com.teo.biplane24.model.Status;
import com.teo.biplane24.ui.GroupFragment;
import com.teo.biplane24.ui.PlaceRagment;
import com.teo.biplane24.ui.RoundFragment;
import com.teo.biplane24.ui.StatusFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @BindView(R.id.period_date_from)
    TextView periodDateFrom;
    @BindView(R.id.period_date_to)
    TextView periodDateTo;

    @BindView(R.id.period_time_from)
    TextView periodTimeFrom;
    @BindView(R.id.period_time_to)
    TextView periodTimeTo;

    @BindView(R.id.layout_day)
    LinearLayout layoutDay;
    @BindView(R.id.day_time_from)
    TextView dayTimeFrom;
    @BindView(R.id.day_time_to)
    TextView dayTimeTo;

    private SharedPreferences preferences;

    private long from;
    private long to;

    private Calendar cFrom;
    private Calendar cTo;
    private Calendar cDay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        int tab = getIntent().getIntExtra("tab", 0);
        if (tab == 2) {
            //layoutDay.setVisibility(View.VISIBLE);
        } else {
            layoutDay.setVisibility(View.GONE);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        from = getIntent().getLongExtra("from", 0);
        to = getIntent().getLongExtra("to", 0);

        cFrom = Calendar.getInstance();
        cFrom.setTimeInMillis(from);
        cTo = Calendar.getInstance();
        cTo.setTimeInMillis(to);

        //from = cFrom.getTimeInMillis() - TimeUnit.DAYS.toMillis(1);
        //to = cTo.getTimeInMillis();

        //cFrom.setTimeInMillis(from);


        periodDateFrom.setText(getFormatDate(cFrom.getTimeInMillis()));
        periodDateTo.setText(getFormatDate(cTo.getTimeInMillis()));

        periodTimeFrom.setText(getFormatTime(cFrom.getTimeInMillis()));
        periodTimeTo.setText(getFormatTime(cTo.getTimeInMillis()));

    }

    @OnClick(R.id.button_save)
    public void save() {

        long from = cFrom.getTimeInMillis();
        long to = cTo.getTimeInMillis();


        Group group = ((GroupFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_group)).getSelect();
        Status status = ((StatusFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_status)).getSelect();
        Place place = ((PlaceRagment) getSupportFragmentManager().findFragmentById(R.id.fragment_place)).getSelect();
        Round round = ((RoundFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_round)).getSelect();

        Intent intent = new Intent();
        intent.putExtra("from", from);
        intent.putExtra("to", to);
        intent.putExtra("group", group.getId());
        intent.putExtra("status", status.getId());
        intent.putExtra("place", place.getId());
        intent.putExtra("round", round.getId());


        Log.e("tr", "intent" + intent.getExtras().toString());
        setResult(RESULT_OK, intent);

        finish();

    }

    private String getFormatDate(long period) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        return format.format(new Date(period));

    }

    private String getFormatTime(long period) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        return format.format(new Date(period));

    }

    @OnClick(R.id.day_from)
    public void fromDay() {

        showDayTimeDialog(true, 0, 0);
    }

    @OnClick(R.id.day_to)
    public void toDay() {
        showDayTimeDialog(true, 0, 0);
    }

    private void showDayTimeDialog(boolean from, int hour, int minute) {

        Calendar calendar = Calendar.getInstance();

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        if (from) {
                            dayTimeFrom.setText(getFormatTime(cFrom.getTimeInMillis()));
                        } else {
                            dayTimeTo.setText(getFormatTime(cFrom.getTimeInMillis()));
                        }

                    }
                }, hour, minute, true);

        timePickerDialog.show();
    }

    @OnClick(R.id.period_from)
    public void from() {
        showFromDateDialog(from, cTo.getTimeInMillis());
    }

    @OnClick(R.id.period_to)
    public void to() {
        showToDateDialog(cFrom.getTimeInMillis(), to);
    }

    private void showFromDateDialog(long min, long max) {

        int mYear = cFrom.get(Calendar.YEAR);
        int mMonth = cFrom.get(Calendar.MONTH);
        int mDay = cFrom.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        cFrom.set(Calendar.YEAR, year);
                        cFrom.set(Calendar.MONTH, monthOfYear);
                        cFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        periodDateFrom.setText(getFormatDate(cFrom.getTimeInMillis()));
                        //txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        showFromTimeDialog();

                    }
                }, mYear, mMonth, mDay);

        //datePickerDialog.getDatePicker().setMaxDate(max);
        //datePickerDialog.getDatePicker().setMinDate(min);
        datePickerDialog.show();
    }

    private void showToDateDialog(long min, long max) {

        int mYear = cTo.get(Calendar.YEAR);
        int mMonth = cTo.get(Calendar.MONTH);
        int mDay = cTo.get(Calendar.DAY_OF_MONTH);

        try {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                            cTo.set(Calendar.YEAR, year);
                            cTo.set(Calendar.MONTH, monthOfYear);
                            cTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                            periodDateTo.setText(getFormatDate(cTo.getTimeInMillis()));

                            showToTimeDialog();
                        }
                    }, mYear, mMonth, mDay);

            //datePickerDialog.getDatePicker().setMaxDate(max);
            //datePickerDialog.getDatePicker().setMinDate(min);
            datePickerDialog.show();
        } catch (Throwable e) {
            e.printStackTrace();
        }


    }


    private void showFromTimeDialog() {


        int mHour = cFrom.get(Calendar.HOUR_OF_DAY);
        int mMinute = cFrom.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


                        cFrom.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        cFrom.set(Calendar.MINUTE, minute);

                        periodTimeFrom.setText(getFormatTime(cFrom.getTimeInMillis()));

                    }
                }, mHour, mMinute, true);

        timePickerDialog.show();
    }

    private void showToTimeDialog() {


        int mHour = cTo.get(Calendar.HOUR_OF_DAY);
        int mMinute = cTo.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        cTo.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        cTo.set(Calendar.MINUTE, minute);

                        periodTimeTo.setText(getFormatTime(cTo.getTimeInMillis()));

                    }
                }, mHour, mMinute, true);

        timePickerDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
