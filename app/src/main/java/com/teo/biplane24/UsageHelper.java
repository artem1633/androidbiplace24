package com.teo.biplane24;

import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.util.Calendar;
import java.util.List;

import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static androidx.core.app.AppOpsManagerCompat.MODE_ALLOWED;

public class UsageHelper {

    private Context context;
    private UsageStatsManager usageStatsManager;

    public UsageHelper(Context context) {
        this.context = context;

    }

    public void init(){
        if (!isPermission(context)) {
            context.startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
            return;
        }

        usageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        qwery();
    }

    public void qwery(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        long start = calendar.getTimeInMillis();
        long end = System.currentTimeMillis();
        List<UsageStats> stats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_WEEKLY, start, end);
        for (UsageStats item : stats){

            //Log.e("tr", "item " +item.getPackageName() + " time: " + item.getFirstTimeStamp() + " - " + item.getLastTimeStamp());
        }
    }


    private boolean isPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }
}
